# Dependencies

- Docker

# Installation

An [Anaconda environment file](environmet.yml) and a [Docker deployment
file](Dockerfile) are included. These automate deployment on an Ubuntu 20.04
container using Anaconda.

The simplest of executions would look like this:

```
$ sudo docker build --tag modularrobots:1.0 .
$ sudo docker run -it --volume /path/on/host/computer:/modularrobots modularrobots:1.0 bash
```

Now the shell is in the container,

```
$ cd modularrobots
$ python main.py --hyperparameters_jsons hyperparameters/HalfCheetah.json
```

## Troubleshooting

Matplotlib is weird when using multiprocessing. Perhaps you need to `$ export
MPLBACKEND=TkAgg` before running the scripts.

