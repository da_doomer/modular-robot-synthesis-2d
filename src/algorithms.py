"""Algorithms used in the project."""
# Native imports
from typing import List
import random
import concurrent.futures
import uuid
from pathlib import Path
from typing import Dict
from typing import Any
import json
import os

# Local imports
from tasks import get_tasks
from policies import Policy
from policies import PolicyContainer
from policies import SafetyAwarePolicy
from multiplexer import PlanningMultiplexer
from multiplexer import FutureVerifyingMultiplexer
from multiplexer import SafetyAwareMultiplexer
from multiplexer import TransitionTrainerMultiplexer
from regions import DecisionTreeSafetyPredictor
from worldmodels import WorldModelFactory
from simulation import get_environment_ins_outs

# External imports
import gym
from stable_baselines3 import PPO
import joblib
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.vec_env import SubprocVecEnv
from stable_baselines3.common.vec_env import VecNormalize
from stable_baselines3.common.monitor import Monitor
import torch.nn as nn
from tqdm import tqdm


def make_env(env_id, task, seed=0):
    def _init():
        env = Monitor(gym.make(env_id, task=task))
        return env
    return _init


def build_policy_on_tasks(
        environment: str,
        tasks: list,
        timesteps: int,
        log_dir: Path,
        hyperparameters: Dict[str, Any],
	parallel_workers_n: int,
        starting_policy: PolicyContainer = None,
        ) -> List[PolicyContainer]:
    # Build a vectorized environment for training,
    # assigning different tasks to different
    # processes.
    num_envs = parallel_workers_n
    #num_envs = 1
    envs = list()
    i = 0
    while len(envs) < num_envs:
        task = tasks[i % len(tasks)]
        envs.append(make_env(environment, task))
        i += 1
    env = SubprocVecEnv(envs, start_method="spawn")
    #env = DummyVecEnv(envs)
    if starting_policy is not None:
        print("Continuing training")
        policy = starting_policy.policy
        env = VecNormalize.load(str(starting_policy.vecnorm_path), env)
        env.reset()
        policy.set_env(env)
    else:
        # Hyperparameters from
        # https://github.com/DLR-RM/rl-baselines3-zoo
        env = VecNormalize(
                env,
                norm_obs=True,
                norm_reward=True,
                clip_obs=10.
            )
        if "policy_kwargs" in hyperparameters.keys()\
                and isinstance(hyperparameters["policy_kwargs"], str):
            hyperparameters["policy_kwargs"] = eval(
                    hyperparameters["policy_kwargs"]
                )
        policy = PPO(
                env=env,
                verbose=1,
                **hyperparameters,
            )

    # Iterate the optimization algorithm through the given
    # number of time-steps
    if timesteps > 0:
        policy.learn(
                timesteps,
                reset_num_timesteps=False,
            )

    env.close()
    policy_path = log_dir/"policy.zip"
    env_path = log_dir/"vecnorm.pkl"
    policy.save(policy_path)
    env.save(env_path)
    policy = PolicyContainer(policy_path, env_path, environment)
    return policy


def build_primitive_policies(
        environment: str,
        timesteps: int,
        log_dir: Path,
        hyperparameters: Dict[str, Any],
        policy_optimization_workers: int,
        executor: concurrent.futures.Executor,
        starting_policies: List[PolicyContainer] = None,
        ) -> Dict[int, PolicyContainer]:
    """Train a policy for each training task of the given environment. It is
    assumed that tasks for the environment are of the following type:

    List[Tuple[float, float]]

    Each training task should have identical values in the second entry. That
    is, for each `0<=i<=len(trainingtasks)` ` it is assumed that:

    set((v for _, v in trainingtasks[i])) == 1

    As a concrete example consider the case where each task is a 1D heightmap,
    so each task is of the form

    task[i] = [(height0, terrainID), (height1, terrainID), ...]

    Each training task should consist of a single terrainID:

    training_tasks = [
        [(h0, FLAT), (h1, FLAT), ...],
        [(h0, BUMPS), (h1, BUMPS), ...],
        ...
        ]
    """
    tasks = get_tasks(environment)
    training_tasks = tasks.train

    # If we are given pre-trained policies to fine-tune, ensure they are
    # as many as the training tasks.
    if starting_policies is not None\
            and len(starting_policies) < len(tasks.train):
        raise ValueError(
                f"Given {len(starting_policies)} pre-trained policies"
                f" for {len(tasks.train)} training tasks"
            )

    policies = dict()
    futures = list()
    for task_i in range(len(training_tasks)):
        task = training_tasks[task_i]
        taskid = set(taskid for _, taskid in task).pop()
        # Build policy
        if starting_policies is not None and taskid in starting_policies.keys():
            starting_policy = starting_policies[taskid]
        else:
            starting_policy = None
        policy_dir = log_dir/f"policy_{task_i}"
        policy_dir.mkdir()
        policy = build_policy_on_tasks(
            environment=environment,
            tasks=[task],
            timesteps=timesteps,
            hyperparameters=hyperparameters,
            log_dir=policy_dir,
            starting_policy=starting_policy,
            parallel_workers_n=policy_optimization_workers,
        )
        policies[taskid] = policy
        print(f"Trained policy for task {taskid}")

    # Return the list of trained policies
    return policies


def build_policy_on_test(
        environment: str,
        timesteps: int,
        log_dir: Path,
        hyperparameters: Dict[str, Any],
	parallel_workers_n: int,
        starting_policy: PolicyContainer = None,
        ) -> PolicyContainer:
    tasks = get_tasks(environment)
    test_tasks = tasks.test

    policy_dir = log_dir/f"policy_{environment}_{uuid.uuid1()}"
    policy_dir.mkdir()
    policy = build_policy_on_tasks(
            environment=environment,
            tasks=test_tasks,
            timesteps=timesteps,
            hyperparameters=hyperparameters,
            log_dir=policy_dir,
            starting_policy=starting_policy,
	    parallel_workers_n=parallel_workers_n,
        )
    return policy


def _sample_rollout(
        environment,
        policy,
        task,
        max_rollout_len,
        perturbation_size=0.0,
        ):
    """This function is specifically made for the
    custom halfcheetah environment."""
    observations = list()
    rollouts_observations = list()
    env = gym.make(environment, task=task)
    obs = env.reset()
    success = True
    if perturbation_size > 0.0:
        state = env.state
        perturbed_state = [
                x + random.random()*perturbation_size
                for x in state
            ]
        env.state = perturbed_state
    rollouts_observations.append(obs)
    info = dict()
    for _ in range(max_rollout_len):
        action = policy(env, obs, info)
        obs, reward, done, info = env.step(action)
        rollouts_observations.append(obs)
        success = env.making_progress
        if done or not success:
            break
    for obs in rollouts_observations:
        observations.append((obs, success))
    return observations


def build_safety_aware_policies(
        policies: Dict[int, Policy],
        environment: str,
        rollouts_n: int,
        max_rollout_len: int,
        log_dir: Path,
        perturbation_size: float,
        executor: concurrent.futures.Executor,
        ) -> Dict[int, SafetyAwarePolicy]:
    # Get tasks for this environment
    tasks = get_tasks(environment)

    # We assume that there is a policy for each training task.
    if len(policies) != len(tasks.train):
        raise ValueError(
                f"The number of policies ({len(policies)})"
                " is different than the number of training tasks"
                f" ({len(tasks.train)}) in environment {environment}"
            )

    # Sample roll-outs to characterize the distribution of successful rollouts.
    # In particular, we assume rollouts in the environment used to train the
    # policy are successful.
    desc = "Characterizing safe regions"
    safety_aware_policies = dict()
    for i, (policy, taskid, task) in tqdm(tuple(enumerate(zip(*policies.items(), tasks.train))), desc=desc):
        observations = list()
        desc = "Sampling rollouts"
        futures = [
                executor.submit(
                    _sample_rollout,
                    environment=environment,
                    policy=policy,
                    task=task,
                    max_rollout_len=max_rollout_len,
                    perturbation_size=perturbation_size,
                )
                for _ in range(rollouts_n)
            ]

        def contains_unsafe_observation(observations):
            return any(not obs_is_safe for _, obs_is_safe in observations)

        def contains_safe_observation(observations):
            return any(obs_is_safe for _, obs_is_safe in observations)

        for future in futures:
            observations.extend(future.result())

        if not contains_safe_observation(observations):
            fake_obs, _ = observations[-1]
            observations.append((fake_obs, True))

        if not contains_unsafe_observation(observations):
            fake_obs, _ = observations[-1]
            observations.append((fake_obs, False))

        success_predictor = DecisionTreeSafetyPredictor()
        success_predictor.fit(observations)
        safety_aware_policy = SafetyAwarePolicy(
                policy=policy,
                safety_predictor=success_predictor,
            )
        safety_aware_policies[taskid] = safety_aware_policy

        # Log the dataset and decision safety predictor
        success_predictor_summary = {
                label: len([obs for obs, l2 in observations if label == l2])
                for label in set(l2 for _, l2 in observations)
            }
        success_predictor_path = log_dir/"success_predictor.joblib"
        success_predictor_observations_path = log_dir/"success_predictor_observations.joblib"
        success_predictor_summary_path = log_dir/"success_predictor_summary.json"
        joblib.dump(success_predictor, success_predictor_path)
        joblib.dump(observations, success_predictor_observations_path)
        with open(success_predictor_summary_path, "wt") as fp:
            json.dump(
                    success_predictor_summary,
                    fp
                )
    return safety_aware_policies


def build_transition_trainer_multiplexer(
        policies: Dict[int, Policy],
        environment: str,
        horizon_len: int,
        transition_training_timesteps: int,
        env_kwargs: dict,
        executor: concurrent.futures.Executor,
        log_dir: Path,
        policy_hyperparameters: Dict[int, Any],
        ) -> FutureVerifyingMultiplexer:
    # Build world model
    worldmodel = WorldModelFactory(
            environment=environment,
        )
    _, action_size = get_environment_ins_outs(environment)

    # Build multiplexer
    multiplexer = TransitionTrainerMultiplexer(
            policies=policies,
            world_model=worldmodel,
            horizon_len=horizon_len,
            action_size=action_size,
            transition_training_timesteps=transition_training_timesteps,
            environment=environment,
            env_kwargs=env_kwargs,
            executor=executor,
            policy_hyperparameters=policy_hyperparameters,
            log_dir=log_dir,
        )
    return multiplexer
