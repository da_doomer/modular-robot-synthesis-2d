"""CMA trajectory optimization."""
# Local imports
from policies import Policy
from policies import ConstantPolicy
from policies import StateMachinePolicy
from worldmodels import WorldModel
from worldmodels import WorldModelFactory
from regions import SafetyPredictor

# Native imports
from typing import List
from typing import Tuple
import os
import copy
import math
import concurrent.futures

# External
import cma
import numpy as np
import gym
from sklearn.utils.extmath import softmax


Trajectory = List[List[float]]


def trajectory_cost(
        trajectory: Trajectory,
        starting_state,
        observation,
        info,
        environment: str,
        env_kwargs: dict,
        safety_predictor: SafetyPredictor = None,
        target_policy: Policy = None,
        target_policy_timesteps: int = 0,
        gamma: float = 0.99
        ) -> float:
    # Make a new instance of the environment, because
    # deepcopy does not create new instances of C objects.
    env = gym.make(environment, **env_kwargs)
    env.reset()
    world_model = WorldModelFactory(
            environment,
        )
    world_model._env = env
    world_model.state = starting_state
    cumulative_reward = 0.0
    _env = env
    _obs = observation
    _info = info
    old_x, _, _ = env.robot.body_xyz
    done = False
    rewards = list()
    for i, action in enumerate(trajectory):
        policy = ConstantPolicy(action)
        _env, _obs, reward, done, _info = world_model.predict(
                _env, _obs, _info, policy
            )
        rewards.append(reward)
        if done:
            break
    # If given, run the target policy
    if target_policy is not None and not done:
        for _ in range(target_policy_timesteps):
            _env, _obs, reward, done, _info = world_model.predict(
                    _env, _obs, _info, target_policy
                )
            rewards.append(reward)
            if done:
                break

    # Compute weighted sum of rewards
    cumulative_reward = sum(gamma**i * r for i, r in enumerate(rewards))
    #cost = -cumulative_reward
    cost = -_env.current_absolute_coordinates[0]
    if safety_predictor is not None:
        final_safety = safety_predictor(_obs)
        cost = cost*max(0.1, final_safety)
    env.close()
    return cost


def receding_horizon(
        horizon_len: int,
        action_size: int,
        starting_state,
        max_rollouts: int,
        sigma: float,
        environment: str,
        env_kwargs: dict,
        executor: concurrent.futures.Executor,
        previous_trajectory: Trajectory = list(),
        observation=None,
        info=None,
        safety_predictor: SafetyPredictor = None,
        target_policy: Policy = None,
        target_policy_timesteps: int = 0,
        control_interval: int = 1,
        ) -> Trajectory:
    """Receding horizon trajectory optimization.

    """
    # Handle resample factor
    original_horizon_len = horizon_len

    def resample(trajectory, control_interval):
        if control_interval == 1:
            return trajectory
        trajectory = np.array(trajectory)
        new_trajectory = list()
        for i in range(len(trajectory)):
            # Linear interpolation for the given control interval
            v1 = trajectory[i]
            if i == len(trajectory)-1:
                v2 = np.array(trajectory[0])
            else:
                v2 = np.array(trajectory[i+1])
            for j in range(control_interval):
                alpha = j/(control_interval)
                v = (1-alpha)*v1 + alpha*v2
                new_trajectory.append(v)
        return np.array(new_trajectory).tolist()

    # Build the initial guess for the optimization
    if len(previous_trajectory) == 0:
        initial_guess = [[0]*action_size]*horizon_len
        max_rollouts = max_rollouts
    else:
        initial_guess = copy.deepcopy(previous_trajectory)
        # Account for the control interval parameter
        for i in range(0, len(previous_trajectory), control_interval):
            initial_guess = initial_guess + [previous_trajectory[i]]
        while len(initial_guess) < guess_len:
            initial_guess = initial_guess + [default_action]

    # Initialize the optimizer
    # TODO parametrize max_workers
    max_workers = 80
    options = {
            "maxfevals": max_rollouts,
            "popsize": max_workers,
            "bounds": [-1, 1],
        }
    optimizer = cma.CMAEvolutionStrategy(
            np.array(initial_guess).flatten(),
            sigma,
            options
        )

    # Optimize the trajectory
    initial_guess_shape = np.array(initial_guess).shape
    while not optimizer.stop():
        solutions = optimizer.ask()
        trajectories = [
                np.reshape(solution, initial_guess_shape)
                for solution in solutions
            ]
        if control_interval is not None:
             trajectories = [
                     resample(t, original_horizon_len)
                     for t in trajectories
                    ]
        futures = [
                executor.submit(
                    trajectory_cost,
                    trajectory=trajectory,
                    starting_state=starting_state,
                    environment=environment,
                    env_kwargs=env_kwargs,
                    observation=observation,
                    info=info,
                    safety_predictor=safety_predictor,
                    target_policy=target_policy,
                    target_policy_timesteps=target_policy_timesteps,
                    )
                for trajectory in trajectories
            ]
        costs = [future.result() for future in futures]
        optimizer.tell(solutions, costs)
        optimizer.disp()

    trajectory = np.reshape(
            optimizer.result.xbest, initial_guess_shape
        )
    if control_interval is not None:
        trajectory = resample(trajectory, original_horizon_len)
    return np.array(trajectory).tolist()
