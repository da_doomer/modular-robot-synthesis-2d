"""MPPI trajectory optimization."""
# Local imports
from policies import Policy
from policies import ConstantPolicy
from policies import StateMachinePolicy
from worldmodels import WorldModel
from worldmodels import WorldModelFactory
from regions import SafetyPredictor

# Native imports
from typing import List
from typing import Tuple
import os
import copy
import math
import concurrent.futures
import time
from multiprocessing import Queue
from multiprocessing import Process

# External
import cma
import numpy as np
import gym
from sklearn.utils.extmath import softmax


Trajectory = List[List[float]]


def trajectory_cost(
        starting_state,
        observation,
        info,
        environment: str,
        env_kwargs: dict,
        task_queue: Queue,
        result_queue: Queue,
        safety_predictor: SafetyPredictor = None,
        target_policy: Policy = None,
        target_policy_timesteps: int = 0,
        gamma: float = 0.99
        ) -> float:
    env = gym.make(environment, **env_kwargs)
    env.reset()
    world_model = WorldModelFactory(
            environment,
        )
    world_model._env = env
  
    task = task_queue.get()
    while task is not None:
        trajectory, trajectory_id = task
        world_model.state = starting_state
        cumulative_reward = 0.0
        _env = env
        _obs = observation
        _info = info
        old_x, _, _ = env.robot.body_xyz
        done = False
        rewards = list()
        t = time.time()
        for i, action in enumerate(trajectory):
            _obs, _reward, done, _info = env.step(action)
            rewards.append(_reward)
            if done:
                break
        tt = time.time() - t
        print(f"{len(trajectory)/tt} steps per second")
        # If given, run the target policy
        if target_policy is not None and not done:
            for i in range(target_policy_timesteps):
                action = target_policy(_obs)
                _obs, _reward, done, _info = env.step(action)
                if i > 0:
                    # Internal Python variables may not immediately reflect
                    # the loaded world state to the simulator
                    rewards.append(_reward)
                if done:
                    break

        # Send costs
        costs = [-r for r in rewards]
        result_queue.put((costs, trajectory_id))
        task = task_queue.get()

    # When the None message is read, re-send for other
    # processes to read
    task_queue.put(None)
    env.close()


def receding_horizon(
        horizon_len: int,
        action_size: int,
        starting_state,
        max_rollouts: int,
        sigma: float,
        environment: str,
        env_kwargs: dict,
        executor: concurrent.futures.Executor,
        previous_trajectory: Trajectory = list(),
        observation=None,
        info=None,
        safety_predictor: SafetyPredictor = None,
        target_policy: Policy = None,
        target_policy_timesteps: int = 0,
        control_interval: int = 1,
        ) -> Trajectory:
    """Model  Predictive  Path  Integral Control

         https://arxiv.org/pdf/1509.01149.pdf
    """
    # TODO parametrize worker_n
    worker_n = 80
    trajectory_n = 1024
    #lambda = 200

    # Handle resample factor
    original_horizon_len = horizon_len
    guess_len = int(horizon_len/control_interval)
    # TODO: parametrize default action
    default_action = [0]*action_size

    def resample(trajectory, control_interval):
        if control_interval == 1:
            return trajectory
        trajectory = np.array(trajectory)
        new_trajectory = list()
        for i in range(len(trajectory)):
            # Linear interpolation for the given control interval
            v1 = trajectory[i]
            if i == len(trajectory)-1:
                v2 = np.array(trajectory[0])
            else:
                v2 = np.array(trajectory[i+1])
            for j in range(control_interval):
                alpha = j/(control_interval)
                v = (1-alpha)*v1 + alpha*v2
                new_trajectory.append(v)
        return np.array(new_trajectory).tolist()

    # Build the initial guess for the optimization
    if len(previous_trajectory) == 0:
        initial_guess = [np.sin([i]*action_size) for i in range(guess_len)]
        max_rollouts = max_rollouts
    else:
        initial_guess = list()
        # Account for the control interval parameter
        for i in range(0, len(previous_trajectory), control_interval):
            initial_guess = initial_guess + [previous_trajectory[i]]
        while len(initial_guess) < guess_len:
            initial_guess = initial_guess + [default_action]

    # Optimize the trajectory
    initial_guess_shape = np.array(initial_guess).shape
    solution = np.array(initial_guess)

    # Sample solutions
    rng = np.random.default_rng()
    solutions = [
        solution + rng.normal(size=(solution.shape))*sigma
        for _ in range(trajectory_n)
    ]

    trajectories = [
            np.reshape(solution, initial_guess_shape)
            for solution in solutions
        ]
    if control_interval is not None:
         trajectories = [
                 resample(t, control_interval)
                 for t in trajectories
                ]

    # Create workers
    task_queue = Queue()
    result_queue = Queue()
    processes = list()

    for i in range(worker_n):
        process_kwargs = dict(
           task_queue=task_queue,
           result_queue=result_queue,
           starting_state=starting_state,
           environment=environment,
           env_kwargs=env_kwargs,
           observation=observation,
           info=info,
           safety_predictor=safety_predictor,
           target_policy=target_policy,
           target_policy_timesteps=target_policy_timesteps,
        )
        p = Process(
           target=trajectory_cost,
           kwargs=process_kwargs
        )
        p.start()
        processes.append(p)

    # Send trajectories to workers
    for i in range(len(trajectories)):
        task = (trajectories[i], i)
        task_queue.put(task)

    # Wait for workers to return costs
    _costs = dict()
    while len(_costs.keys()) < len(trajectories):
        cost, i = result_queue.get()
        _costs[i] = cost
    costs = [_costs[i] for i in range(len(_costs.keys()))]

    # Terminate workers
    task_queue.put(None)
    for i in range(len(processes)):
        processes[i].join()

    # Compute the trajectory
    costs = np.array(costs)
    costs = (costs + min(costs.flatten()))/(max(costs.flatten())-min(costs.flatten()))
    original_solution = np.array(solution)
    for i in range(len(solution)):
        step_costs = [
            np.exp(-costs[k][i])
            for k in range(len(costs))
        ]
        sum_costs = sum(step_costs)
        for k in range(len(costs)):
            d_uik = solutions[k][i] - original_solution[i]
            cost = step_costs[k]
            solution[i] += cost/sum_costs*d_uik

    # Return optimal trajectory
    trajectory = np.reshape(
            solution, initial_guess_shape
        )
    if control_interval is not None:
        trajectory = resample(trajectory, control_interval)
    return trajectory
