import gym
import gym.wrappers
import random
import envs
# Doing manual recording because gym.wrappers.Monitor hangs up on line 307
from PIL import Image
import ffmpeg
import concurrent.futures

# Register custom environments
import sys
from pathlib import Path
sys.path.append(str(Path('.').absolute()))
gym.logger.set_level(40)
gym.envs.register(
    id="HalfCheetahLabeledBulletTerrainEnv-v0",
    entry_point="envs:HalfCheetahLabeledBulletTerrainEnv",
)
gym.envs.register(
    id="HopperLabeledBulletTerrainEnv-v0",
    entry_point="envs:HopperLabeledBulletTerrainEnv",
)


def get_environment_ins_outs(envname: str):
    env = gym.make(envname)
    ins = len(env.observation_space.sample())
    outs = len(env.action_space.sample())
    return ins, outs


def simulate(
        envname,
        policy,
        steps: int = None,
        seed: int = None,
        recording_dir: Path = None,
        env_kwargs: dict = dict(),
        show_pbar: bool = False,
        fps: int = 60,
        executor: concurrent.futures.Executor = None,  # for parallel saving of frame images
        ):
    if seed is None:
        seed = random.randint(0, 1000000)

    env = gym.make(envname, **env_kwargs)
    env_unwrapped = env
    env.seed(seed)
    observation = env.reset()

    def get_filename(i: int) -> Path:
        idstr = str(i).rjust(20, "0")
        return (recording_dir/idstr).with_suffix(".png")

    if steps is None:
        steps = float("inf")
    step_i = 0
    rewards = list()
    info = dict()
    framepaths = list()
    futures = list()
    filenames = list()
    with policy:
        while step_i < steps:
            action = policy(env_unwrapped, observation, info)
            observation, reward, done, info = env.step(action)
            # Save frame
            if recording_dir is not None:
                frame = env.render(mode="rgb_array")
                image = Image.fromarray(frame)
                filename = get_filename(step_i)
                filenames.append(filename)
                if executor is not None:
                    futures.append(executor.submit(image.save, filename))
                else:
                    image.save(filename)
            rewards.append(reward)
            if done:
                break
            step_i += 1
    env.close()

    # Concatenate images into mp4 movie
    for future in futures:
        future.result()
    if recording_dir is not None:
        filename = (recording_dir/"movie").with_suffix(".mp4")
        (
	    ffmpeg
	    .input(recording_dir/"*.png", pattern_type="glob", framerate=fps)
	    .output(str(filename))
	    .run()
         )
    for filename in filenames:
        filename.unlink()
    return rewards, policy.logs
