# TODO parametrize all remaining approaches with `trajectory_optimization_control_interval`
# Native imports
import argparse
import json
from pathlib import Path
from datetime import datetime
from typing import List
from typing import Dict
from typing import Any
import concurrent.futures
import multiprocessing
import random
import shutil

# Local imports
import logs
import simulation
from tasks import get_tasks
from tasks import Task
from algorithms import build_primitive_policies
from algorithms import build_safety_aware_policies
from algorithms import build_policy_on_test
from algorithms import build_transition_trainer_multiplexer
from policies import Policy
from policies import PolicyContainer
from policies import RecedingHorizonPolicy
from policies import RecedingHorizonStateMachineLocomotionPolicy
from policies import load_policy
from policies.statemachine import get_predicates
from multiplexer import PlanningMultiplexer
from multiplexer import TransitionTrainerMultiplexer
from multiplexer import AbruptMultiplexer
from multiplexer import AbruptPlanningMultiplexer
from multiplexer import FutureVerifyingMultiplexer
from multiplexer import TransitionPolicyMultiplexer
from worldmodels import WorldModelFactory
import simulation


def direct_approach(
        environment: str,
        safety_rollouts_n: int,
        max_rollout_len: int,
        trajectory_optimization_sigma: float,
        horizon_len: int,
        log_dir: Path,
        trajectory_optimization_rollouts: int,
        primitive_policies: Dict[int, Policy],
        executor: concurrent.futures.Executor,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Optimize transitions between primitive policies."""
    # Log the multiplexer
    tasks = get_tasks(environment)
    rewards = dict()
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }

        # Build a multiplexer
        worldmodel = WorldModelFactory(
                environment=environment,
            )
        _, action_size = simulation.get_environment_ins_outs(environment)
        multiplexer = PlanningMultiplexer(
                policies=primitive_policies,
                world_model=worldmodel,
                horizon_len=horizon_len,
                action_size=action_size,
                max_rollouts=trajectory_optimization_rollouts,
                environment=environment,
                env_kwargs=environment_kwargs,
                executor=executor,
                trajectory_optimization_sigma=trajectory_optimization_sigma,
            )

        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=multiplexer,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def future_verification_approach(
        environment: str,
        safety_rollouts_n: int,
        max_rollout_len: int,
        trajectory_optimization_sigma: float,
        horizon_len: int,
        log_dir: Path,
        trajectory_optimization_rollouts: int,
        primitive_policies: Dict[int, Policy],
        executor: concurrent.futures.Executor,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Optimize transitions between primitive policies explicitly running the
    target policy at the end of the trajectory to check for good performance."""
    # Log the multiplexer
    tasks = get_tasks(environment)
    rewards = dict()
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }

        # Build a future verifying multiplexer
        worldmodel = WorldModelFactory(
                environment=environment,
            )
        _, action_size = simulation.get_environment_ins_outs(environment)
        multiplexer = FutureVerifyingMultiplexer(
                policies=primitive_policies,
                world_model=worldmodel,
                horizon_len=horizon_len,
                action_size=action_size,
                max_rollouts=trajectory_optimization_rollouts,
                environment=environment,
                env_kwargs=environment_kwargs,
                executor=executor,
                trajectory_optimization_sigma=trajectory_optimization_sigma,
            )

        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=multiplexer,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def safe_approach(
        environment: str,
        safety_rollouts_n: int,
        max_rollout_len: int,
        trajectory_optimization_sigma: float,
        horizon_len: int,
        log_dir: Path,
        trajectory_optimization_rollouts: int,
        state_perturbation_size: float,
        primitive_policies: Dict[int, Policy],
        executor: concurrent.futures.Executor,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Optimize transitions between primitive policies taking into account
    their "safe" regions."""
    # Add safety awareness to the policies
    safety_aware_policies = build_safety_aware_policies(
                policies=primitive_policies,
                environment=environment,
                rollouts_n=safety_rollouts_n,
                max_rollout_len=max_rollout_len,
                perturbation_size=state_perturbation_size,
                log_dir=log_dir,
                executor=executor,
            )

    # Log the multiplexer
    tasks = get_tasks(environment)
    rewards = dict()
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }

        # Build a safety aware multiplexer
        worldmodel = WorldModelFactory(
                environment=environment,
            )
        _, action_size = get_environment_ins_outs(environment)
        multiplexer = SafetyAwareMultiplexer(
                policies=safety_aware_policies,
                world_model=worldmodel,
                horizon_len=horizon_len,
                action_size=action_size,
                max_rollouts=trajectory_optimization_rollouts,
                environment=environment,
                env_kwargs=env_kwargs,
                executor=executor,
            )

        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=multiplexer,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def abrupt_approach(
        environment: str,
        log_dir: Path,
        primitive_policies: Dict[int, Policy],
        max_rollout_len: int,
        executor: concurrent.futures.Executor,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Hard-switch between policies."""
    # Log the multiplexer
    tasks = get_tasks(environment)
    rewards = dict()
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }

        # Build a safety aware multiplexer
        multiplexer = AbruptMultiplexer(
                policies=primitive_policies,
            )

        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=multiplexer,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def abrupt_planning_approach(
        environment: str,
        log_dir: Path,
        primitive_policies: Dict[int, Policy],
        horizon_len: int,
        max_rollout_len: int,
        executor: concurrent.futures.Executor,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Planning when to hard-switch between policies."""
    tasks = get_tasks(environment)
    rewards = dict()

    # Build the planning policy
    world_model = WorldModelFactory(environment)
    _, action_size = simulation.get_environment_ins_outs(environment)
    policy = AbruptPlanningMultiplexer(
            policies=primitive_policies,
            world_model=world_model,
            horizon_len=horizon_len,
            environment=environment,
            env_kwargs=dict(),
            executor=executor,
        )

    # Log the multiplexer
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }
        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=policy,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def rl_on_test_approach(
        environment: str,
        monolithic_policy_optimization_timesteps: int,
        max_rollout_len: int,
        log_dir: Path,
        continue_training: bool,
        executor: concurrent.futures.Executor,
        policy_optimization_hyperparameters: Dict[str, Any],
        parallel_workers_n: int,
        skip_train_plots: bool,
        monolithic_policy: PolicyContainer = None,
        **kwargs,
        ) -> Dict[Task, float]:
    """Train a policy on the test terrains."""
    # Train a policy on the test terrains
    tasks = get_tasks(environment)
    if monolithic_policy is None\
            or (monolithic_policy is not None and continue_training):
        policy = build_policy_on_test(
                environment=environment,
                timesteps=monolithic_policy_optimization_timesteps,
                hyperparameters=policy_optimization_hyperparameters,
                log_dir=log_dir,
                starting_policy=monolithic_policy,
                parallel_workers_n=parallel_workers_n,
            )
    else:
        policy = monolithic_policy

    # Log the policy
    rewards = dict()
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task, }

        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=policy,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def rl_on_transitions_approach(
        environment: str,
        max_rollout_len: int,
        horizon_len: int,
        log_dir: Path,
        transition_training_timesteps: int,
        primitive_policies: Dict[int, Policy],
        executor: concurrent.futures.Executor,
        policy_optimization_hyperparameters: Dict[int, Any],
        policy_optimization_workers: int,
        transition_policies_training_tasks: int,
        finish_policy_timesteps: int,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Optimize a transition policy for each source-target policy pair.

    Primitive policies are executed until a transition is detected in the
    planning horizon. At that point the corresponding source-target policy is
    trained with an RL algorithm over an environment defined as follows:

        - s_0: the state at which the transition was detected
        - horizon_len*2: maximum timesteps
        - transition function is defined by forward simulation

        """
    tasks = get_tasks(environment)
    rewards = dict()
    n = transition_policies_training_tasks

    # Build the multiplexer
    worldmodel = WorldModelFactory(
            environment=environment,
        )
    _, action_size = simulation.get_environment_ins_outs(environment)

    # Build multiplexer
    multiplexer = TransitionTrainerMultiplexer(
            policies=primitive_policies,
            world_model=worldmodel,
            horizon_len=horizon_len,
            action_size=action_size,
            transition_training_timesteps=transition_training_timesteps,
            environment=environment,
            env_kwargs=env_kwargs,
            executor=executor,
            policy_hyperparameters=policy_optimization_hyperparameters,
            policy_optimization_workers=policy_optimization_workers,
            log_dir=log_dir,
            finish_policy_timesteps=finish_policy_timesteps,
        )

    # Train the transition policies
    multiplexer.training = True
    for i in range(n):
        print(f"Training transitions {i}/{transition_policies_training_tasks}")
        task = random.choice(tasks.test)
        env_kwargs = {
                "task": task
            }
        simulation.simulate(environment, multiplexer, env_kwargs=env_kwargs)
    multiplexer.training = False

    # Log the multiplexer
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }
        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=multiplexer,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def receding_horizon_approach(
        environment: str,
        horizon_len: int,
        trajectory_optimization_rollouts: int,
        trajectory_optimization_sigma: float,
        log_dir: Path,
        executor: concurrent.futures.Executor,
        max_rollout_len: int,
        trajectory_optimization_control_interval: float,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Optimization over the space of actions over finite horizons."""
    tasks = get_tasks(environment)
    rewards = dict()

    # Build the planning policy
    world_model = WorldModelFactory(environment)
    _, action_size = simulation.get_environment_ins_outs(environment)
    policy = RecedingHorizonPolicy(
            world_model=world_model,
            horizon_len=horizon_len,
            max_rollouts=trajectory_optimization_rollouts,
            environment=environment,
            env_kwargs=dict(),
            executor=executor,
            action_size=action_size,
            trajectory_optimization_sigma=trajectory_optimization_sigma,
            trajectory_optimization_control_interval=trajectory_optimization_control_interval,
        )

    # Log the multiplexer
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }
        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=policy,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def receding_horizon_state_machine_approach(
        environment: str,
        horizon_len: int,
        trajectory_optimization_rollouts: int,
        trajectory_optimization_sigma: float,
        log_dir: Path,
        executor: concurrent.futures.Executor,
        max_rollout_len: int,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Optimization over actions of a state machine. The transition
    predicates are hardcoded as in previous work.


    Bin Peng et al, Terrain-Adaptive Locomotion Skills Using Deep Reinforcement Learning, 2016
    """
    tasks = get_tasks(environment)
    rewards = dict()

    # Build the planning policy
    world_model = WorldModelFactory(environment)
    _, action_size = simulation.get_environment_ins_outs(environment)
    predicates = get_predicates(environment)
    policy = RecedingHorizonStateMachineLocomotionPolicy(
            world_model=world_model,
            horizon_len=horizon_len,
            max_rollouts=trajectory_optimization_rollouts,
            environment=environment,
            env_kwargs=dict(),
            executor=executor,
            action_size=action_size,
            predicates=predicates,
            trajectory_optimization_sigma=trajectory_optimization_sigma,
        )

    # Log the multiplexer
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }
        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=policy,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def transition_policy_approach(
        environment: str,
        horizon_len: int,
        primitive_policies: Dict[int, Policy],
        monolithic_policy: PolicyContainer,
        log_dir: Path,
        executor: concurrent.futures.Executor,
        max_rollout_len: int,
        skip_train_plots: bool,
        **kwargs,
        ) -> Dict[Task, float]:
    """Optimization over actions of a state machine. The transition
    predicates are hardcoded as in previous work.


    Bin Peng et al, Terrain-Adaptive Locomotion Skills Using Deep Reinforcement Learning, 2016
    """
    tasks = get_tasks(environment)
    rewards = dict()

    # Build the policy
    policy = TransitionPolicyMultiplexer(
        policies=primitive_policies,
        transition_policy=monolithic_policy,
        horizon_len=horizon_len,
        )

    # Log the multiplexer
    for i, task in enumerate(tasks):
        if task in tasks.train and skip_train_plots: continue
        environment_kwargs = {
                "task": task,
            }
        task_log_dir = log_dir/f"task_{i}"
        task_log_dir.mkdir()
        rewards_ = logs.log_policy(
                policy=policy,
                environment=environment,
                log_dir=task_log_dir,
                environment_kwargs=environment_kwargs,
                max_rollout_len=max_rollout_len,
                executor=executor,
            )
        rewards[task] = sum(rewards_)
    return rewards


def get_approaches_rewards(
        hyperparameters_json: Path,
        policy_library_dir: Path,
        monolithic_policy_dir: Path,
        continue_training: bool,
        log_dir: Path,
        ):
    """Performs the experiment for the given configuration, returning the
    rewards for each approach.

    Specifically, the return value is a mapping:

        approach_name -> (task -> float)
    """
    # Open and log hyperparameter file
    shutil.copy(hyperparameters_json, log_dir/hyperparameters_json.name)
    with open(hyperparameters_json) as f:
        hyperparameters_json = json.load(f)
    environment = hyperparameters_json["environment"]
    policy_optimization_hyperparameters = hyperparameters_json[
            "policy_optimization_hyperparameters"
        ]
    policy_optimization_timesteps = hyperparameters_json[
            "policy_optimization_timesteps"
        ]
    policy_optimization_workers = hyperparameters_json[
            "policy_optimization_workers"
        ]
    parallel_workers_n = hyperparameters_json[
            "parallel_workers_n"
        ]

    # Create worker processes
    ctx = multiprocessing.get_context("forkserver")
    #executor = concurrent.futures.ProcessPoolExecutor(mp_context=ctx)
    executor = concurrent.futures.ThreadPoolExecutor(parallel_workers_n)
    #executor = concurrent.futures.ProcessPoolExecutor(parallel_workers_n, mp_context=ctx)

    # Open primitive policies
    primitive_policies = None
    if policy_library_dir is not None:
        policy_paths = sorted(
                    policy_library_dir.glob("**/policy.zip")
                )
        vecnorm_paths = sorted(
                     policy_library_dir.glob("**/vecnorm.pkl")
                )
        _primitive_policies = [
                load_policy(policy_path, vecnorm_path, environment)
                for policy_path, vecnorm_path
                in zip(policy_paths, vecnorm_paths)
            ]
        tasks = get_tasks(environment)
        taskids = (
                set(taskid for _, taskid in task).pop()
                for task in tasks.train
            )
        primitive_policies = dict(zip(taskids, _primitive_policies))

    # If no policies given, train from scratch. Or if specified, continue
    # retraining policies
    primitive_policies_dir = log_dir/"primitive_policies"
    primitive_policies_dir.mkdir()
    if primitive_policies is not None and continue_training:
        primitive_policies = build_primitive_policies(
                environment=environment,
                timesteps=policy_optimization_timesteps,
                starting_policies=primitive_policies,
                hyperparameters=policy_optimization_hyperparameters,
                log_dir=primitive_policies_dir,
                policy_optimization_workers=policy_optimization_workers,
                executor=executor,
            )
    elif primitive_policies is None:
        primitive_policies = build_primitive_policies(
                environment=environment,
                timesteps=policy_optimization_timesteps,
                hyperparameters=policy_optimization_hyperparameters,
                log_dir=primitive_policies_dir,
                policy_optimization_workers=policy_optimization_workers,
                executor=executor,
            )
    else:
        # Save the policies
        for i, policy in enumerate(primitive_policies.values()):
            policy_dir = primitive_policies_dir/f"policy_{i}"
            policy_dir.mkdir()
            policy_path = policy_dir/"policy.zip"
            vecnorm_path = policy_dir/"vecnorm.pkl"
            policy.policy.save(policy_path)
            policy.vecnorm.save(vecnorm_path)

    # Open monolithic policy
    monolithic_policy = None
    if monolithic_policy_dir is not None:
        local_monolithic_policy_dir = log_dir/"monolithic_policy"
        shutil.copytree(monolithic_policy_dir, local_monolithic_policy_dir)
        policy_path = list(local_monolithic_policy_dir.glob("**/policy.zip"))[0]
        vecnorm_path = list(local_monolithic_policy_dir.glob("**/vecnorm.pkl"))[0]
        monolithic_policy = load_policy(
                policy_path,
                vecnorm_path,
                environment,
            )

    # Define approaches
    approaches = [
            #abrupt_approach,
            #abrupt_planning_approach,
            #direct_approach,
            #future_verification_approach,
            #safe_approach,
            rl_on_test_approach,
            #rl_on_transitions_approach,
            #receding_horizon_approach,
            #receding_horizon_state_machine_approach,
            #transition_policy_approach,
        ]

    # Execute all approaches and get corresponding rewards
    approaches_rewards = dict()
    for approach in approaches:
        name = approach.__name__
        approach_dir = log_dir/name
        approach_dir.mkdir()
        rewards = approach(
                log_dir=approach_dir,
                primitive_policies=primitive_policies,
                monolithic_policy=monolithic_policy,
                continue_training=continue_training,
                executor=executor,
                **hyperparameters_json
            )
        approaches_rewards[name] = rewards

    # Plot approach rewards
    logs.log_approaches_rewards(
            approaches_rewards=approaches_rewards,
            environment=environment,
            log_dir=log_dir,
            )

    # Stop worker processes
    executor.shutdown()
    return approaches_rewards


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Run the pipeline for every given JSON.",
        )
    parser.add_argument(
            "--hyperparameters_jsons",
            help="JSON files with the hyperparameters for the experiment.",
            type=Path,
            required=True,
            nargs="+",
        )
    parser.add_argument(
            "--starting_policies_dir",
            help="Directory with pre-trained starting policies.",
            type=Path,
            default=None,
        )
    parser.add_argument(
            "--primitive_policy_dir",
            help="Directory with pre-trained primitive policies (search recursively).",
            type=Path,
            default=None,
        )
    parser.add_argument(
            "--monolithic_policy_dir",
            help="Pre-trained monolothic policy.",
            type=Path,
            default=None,
        )
    parser.add_argument(
            "--continue_training",
            help="Continue training the pre-trained primitive policies.",
            action="store_const",
            const=True,
            default=False,
        )
    args = parser.parse_args()

    # Read arguments
    hyperparameters_jsons = args.hyperparameters_jsons
    policy_library_dir = args.primitive_policy_dir
    monolithic_policy_dir = args.monolithic_policy_dir
    continue_training = args.continue_training

    # Ensure pre-trained policies are only given when a single experiment
    # is to be performed.
    pretrained_args = (
            policy_library_dir,
            monolithic_policy_dir,
            continue_training,
        )
    if len(hyperparameters_jsons) > 1 and any(pretrained_args):
        raise ValueError(
            "Pre-trained policy arguments can only be given with a single"
            " hyperparameter argument."
        )

    # Create log dir
    log_dir = Path("experiments")/f"{str(datetime.now())}"
    log_dir.mkdir(parents=True)

    pipeline_rewards = dict()
    for hyperparameters_json in hyperparameters_jsons:
        experiment_id = hyperparameters_json.stem
        experiment_log_dir = log_dir/experiment_id
        experiment_log_dir.mkdir()
        approaches_rewards = get_approaches_rewards(
                hyperparameters_json=hyperparameters_json,
                policy_library_dir=policy_library_dir,
                monolithic_policy_dir=monolithic_policy_dir,
                continue_training=continue_training,
                log_dir=experiment_log_dir,
            )
