"""Defines a common interface for policies."""
# External imports
import numpy as np
from stable_baselines3 import PPO
from stable_baselines3.common.vec_env import VecNormalize
from stable_baselines3.common.vec_env import DummyVecEnv
import gym

# Native imports
import abc
from collections import defaultdict
from pathlib import Path
import copy
from typing import Tuple

# Local imports
from regions import SafetyPredictor


class Policy(abc.ABC):
    def __init__(self):
        super().__init__()
        self.logs = defaultdict(list)

    def __enter__(self):
        self.logs = defaultdict(list)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def log(self, key: str, value):
        self.logs[key].append(np.array(value).flatten())

    @abc.abstractmethod
    def __call__(self, env, observation, info):
        pass


class PolicyContainer(Policy):
    """Stable-baselines3 container. This is meant to hold a frozen policy.
    Receives a path to allow pickling (needed for multiprocessing using the
    standard library)."""
    def __init__(
            self,
            policy_path: Path,
            vecnorm_path: Path,
            environment: str,
            ):
        super().__init__()
        self.policy_path = policy_path
        self.vecnorm_path = vecnorm_path
        self.environment = environment
        self._policy = None
        self._vecnorm = None

    @property
    def policy(self):
        if self._policy is None:
            self._policy = PPO.load(str(self.policy_path))
        return self._policy

    @property
    def vecnorm(self):
        if self._vecnorm is None:
            vecenv = DummyVecEnv([lambda: gym.make(self.environment)])
            self._vecnorm = VecNormalize.load(str(self.vecnorm_path), vecenv)
        return self._vecnorm

    def __call__(
            self,
            env,
            observation,
            info,
            ):
        observation = self.vecnorm.normalize_obs(observation)
        action = self.policy.predict(observation, deterministic=True)[0]
        return action

    def __getstate__(self):
        self._policy = None
        self._vecnorm = None
        d = copy.deepcopy(self.__dict__)
        return d


class SafetyAwarePolicy(Policy):
    def __init__(
            self,
            policy: Policy,
            safety_predictor: SafetyPredictor,
            ):
        super().__init__()
        self.policy = policy
        self.safety_predictor = safety_predictor

    def safety(self, observation) -> float:
        return self.safety_predictor(observation)

    def __call__(
            self,
            env,
            observation,
            info,
            ):
        action = self.policy(env, observation, info)
        safety = self.safety_predictor(observation)
        self.log("safety", [safety])
        return action


class ConstantPolicy:
    def __init__(
            self,
            action,
            ):
        super().__init__()
        self.action = action

    def __call__(
            self,
            env,
            observation,
            info,
            ):
        return self.action


def load_policy(
        path: Path, vecnorm: Path, environment: str
        ) -> PolicyContainer:
    """Load an stable-baselines3 policy."""
    return PolicyContainer(path, vecnorm, environment)
