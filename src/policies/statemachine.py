"""A state machine policy."""
from . import Policy
from typing import List
from . import ConstantPolicy


class StateMachinePolicy(Policy):
    """A state machine indexed with integers. A transition is taken when
    the output of the corresponding predicate is strictly greater than zero.
    Predicates are policies whose actions are single floating point numbers.
    """
    def __init__(
            self,
            policies: List[Policy],
            predicates: List[List[Policy]],
            initial_state = 0
            ):
        super().__init__()
        self.policies = policies
        self.predicates = predicates
        self.policy_i = initial_state
        self.initial_state = initial_state

    def __enter__(self):
        super().__enter__()
        self.timestep = 0
        self.policy_i = self.initial_state

    def __call__(self, env, obs, info):
        for j, predicate in enumerate(self.predicates[self.policy_i]):
            if predicate(env, obs, info) > 0:
                self.policy_i = j
        action = self.policies[self.policy_i](env, obs, info)
        return action


################# Bullet locomotion predicates
# For details see:
# Terrain-Adaptive Locomotion Skills Using Deep Reinforcement Learning
# 2016

class HalfCheetahBackStanceToExtend(Policy):
    def __call__(self, env, obs, info):
        # Change to back stance exactly when the back foot
        # leaves the ground
        i = env.robot.foot_list.index("bfoot")
        contact = env.robot.feet_contact[i]
        if not contact:
            return 1.0
        return 0.0


class HalfCheetahExtendToFront(Policy):
    def __call__(self, env, obs, info):
        # Change to front foot contact exactly when the front foot
        # touches the ground
        i = env.robot.foot_list.index("ffoot")
        contact = env.robot.feet_contact[i]
        if contact:
            return 1.0
        return 0.0


class HalfCheetahFrontToBackContact(Policy):
    def __call__(self, env, obs, info):
        # Change to back foot contact exactly when the front foot
        # leaves the ground
        i = env.robot.foot_list.index("ffoot")
        contact = env.robot.feet_contact[i]
        if not contact:
            return 1.0
        return 0.0


class HalfCheetahBackContactToBackStance(Policy):
    def __call__(self, env, obs, info):
        # Change to back stance exactly when the back foot
        # touches the ground
        i = env.robot.foot_list.index("bfoot")
        contact = env.robot.feet_contact[i]
        if contact:
            return 1.0
        return 0.0


def get_predicates(env: str) -> List[List[Policy]]:
    if "HalfCheetah" in env:
        zero = ConstantPolicy(0.0)
        predicates = [
            [
                zero,
                HalfCheetahBackStanceToExtend(),
                zero,
                zero
            ],
            [
                zero,
                zero,
                HalfCheetahExtendToFront(),
                zero
            ],
            [
                zero,
                zero,
                zero,
                HalfCheetahFrontToBackContact()
            ],
            [
                HalfCheetahBackContactToBackStance(),
                zero,
                zero,
                zero
            ],
        ]
        return predicates
    raise ValueError(f"No predicates defined for environment {env}")
