"""A policy that employs a world model to do online planning."""
from . import Policy
import optimization
from worldmodels import WorldModel
import concurrent.futures
from typing import List
from . import ConstantPolicy
from . import StateMachinePolicy


class RecedingHorizonPolicy(Policy):
    def __init__(
            self,
            horizon_len: int,
            max_rollouts: int,
            environment: str,
            env_kwargs: dict,
            action_size: int,
            world_model: WorldModel,
            trajectory_optimization_sigma: int,
            trajectory_optimization_control_interval: float,
            executor: concurrent.futures.Executor,
            ):
        self.horizon_len = horizon_len
        self.max_rollouts = max_rollouts
        self.environment = environment
        self.env_kwargs = env_kwargs
        self.executor = executor
        self.action_size = action_size
        self.world_model = world_model
        self.trajectory_optimization_sigma = trajectory_optimization_sigma
        self.trajectory_optimization_control_interval = trajectory_optimization_control_interval
        self.plan = list()

    def __enter__(self):
        super().__enter__()
        self.timestep = 0
        self.plan = list()

    def __call__(self, env, obs, info):
        # Because the world model is perfect (in this case), we can
        # perform the "receding horizon" once every `horizon_len` timesteps.
        self.world_model._env = env.unwrapped
        current_state = self.world_model.state
        self.plan = optimization.cma_receding_horizon(
            horizon_len=self.horizon_len,
            action_size=self.action_size,
            sigma=self.trajectory_optimization_sigma,
            starting_state=current_state,
            max_rollouts=self.max_rollouts,
            environment=self.environment,
            env_kwargs=self.env_kwargs,
            executor=self.executor,
            previous_trajectory=self.plan,
            observation=obs,
            info=info,
            control_interval=self.trajectory_optimization_control_interval,
        )
        action = self.plan.pop(0)
        self.plan.append(action)
        # TODO remove debugging output
        self.timestep += 1
        print(f"{self.timestep}, {env.unwrapped.robot.body_xyz}")
        self.log("observation", obs)
        self.log("action", action)
        return action


class RecedingHorizonStateMachineLocomotionPolicy(Policy):
    """Fixed predicates."""
    def __init__(
            self,
            horizon_len: int,
            max_rollouts: int,
            environment: str,
            env_kwargs: dict,
            action_size: int,
            world_model: WorldModel,
            trajectory_optimization_sigma: int,
            predicates: List[List[Policy]],
            executor: concurrent.futures.Executor,
            ):
        self.horizon_len = horizon_len
        self.max_rollouts = max_rollouts
        self.environment = environment
        self.env_kwargs = env_kwargs
        self.executor = executor
        self.action_size = action_size
        self.world_model = world_model
        self.trajectory_optimization_sigma = trajectory_optimization_sigma
        self.predicates = predicates
        self.default_policies = [ConstantPolicy([0.0]*action_size) for _ in range(4)]
        self.policy = StateMachinePolicy(
            policies=self.default_policies,
            predicates=predicates,
        )

    def __enter__(self):
        super().__enter__()
        self.timestep = 0
        self.plan = list()
        self.policy = StateMachinePolicy(
            policies=self.default_policies,
            predicates=self.predicates,
        )

    def __call__(self, env, obs, info):
        #if self.timestep % self.horizon_len == 0:
        if self.timestep == 0:
            # Optimize state machine's actions
            self.world_model._env = env.unwrapped
            current_state = self.world_model.state
            self.policy = optimization.optimize_state_machine_policies(
                horizon_len=self.horizon_len,
                action_size=self.action_size,
                sigma=self.trajectory_optimization_sigma,
                starting_state=current_state,
                max_rollouts=self.max_rollouts,
                environment=self.environment,
                env_kwargs=self.env_kwargs,
                executor=self.executor,
                predicates=self.predicates,
                observation=obs,
                info=info,
            )

        action = self.policy(env, obs, info)
        # TODO remove debugging output
        self.timestep += 1
        print(f"{self.timestep}, {env.unwrapped.robot.body_xyz}")
        self.log("observation", obs)
        self.log("action", action)
        self.log("state_i", self.policy.policy_i)
        return action
