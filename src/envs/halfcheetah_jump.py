"""Jumping over gaps pybullet environments. The implementation relies
on sub-classing pybullet environments to avoid setting joint torques
by hand."""
# Native imports
from typing import List
from typing import Tuple
from collections import deque
import abc
import json

# External imports
import gym
import numpy as np

# PyBullet spams stdout upon import. The workaround is
# complex but otherwise messages are lost in the spam.
# https://github.com/bulletphysics/bullet3/discussions/3441
import sys
import ctypes
import os
class RedirectStream(object):

  @staticmethod
  def _flush_c_stream(stream):
    streamname = stream.name[1:-1]
    libc = ctypes.CDLL(None)
    libc.fflush(ctypes.c_void_p.in_dll(libc, streamname))

  def __init__(self, stream=sys.stdout, file=os.devnull):
    self.stream = stream
    self.file = file

  def __enter__(self):
    self.stream.flush()  # ensures python stream unaffected 
    self.fd = open(self.file, "w+")
    self.dup_stream = os.dup(self.stream.fileno())
    os.dup2(self.fd.fileno(), self.stream.fileno()) # replaces stream
  
  def __exit__(self, type, value, traceback):
    RedirectStream._flush_c_stream(self.stream)  # ensures C stream buffer empty
    os.dup2(self.dup_stream, self.stream.fileno()) # restores stream
    os.close(self.dup_stream)
    self.fd.close()

import pybullet
import pybullet_envs
import pybullet_envs.gym_locomotion_envs
import pybullet_envs.robot_locomotors
import pybullet_envs.scene_abstract


class RunningMean:
    """Simple class to keep track of the running mean succinctly. This is
    compatible with any Python type supporting addition and division."""
    def __init__(self, start_value):
        self.mean = start_value
        self.n = 1

    def add(self, item):
        self.mean = (self.mean*self.n + item)/(self.n+1.0)
        self.n += 1


def add_heightmap_to_simulation(
        p,
        heightmap: List[List[float]],
        x_scale,
        y_scale,
        z_scale,
        ):
    heightmap = np.array(heightmap).T
    rows_n = len(heightmap[0])
    cols_n = len(heightmap)
    flatmap = [col for row in heightmap for col in row]
    terrainShape = p.createCollisionShape(
            shapeType=p.GEOM_HEIGHTFIELD,
            meshScale=[x_scale, y_scale, z_scale],
            heightfieldTextureScaling=(rows_n-1)/2,
            heightfieldData=flatmap,
            numHeightfieldRows=rows_n,
            numHeightfieldColumns=cols_n,
        )
    terrainid = p.createMultiBody(0, terrainShape)
    # Bullet recenters the map based on its min and max heights.
    z_shift = (max(flatmap)+min(flatmap))/2*z_scale
    # Shift the map to the center
    y_shift = 0
    p.resetBasePositionAndOrientation(
            terrainid,
            [0, y_shift, z_shift],
            [0, 0, 0, 1]
        )
    return terrainid


class HeightmapScene(pybullet_envs.scene_abstract.Scene):
    multiplayer = False

    def __init__(
            self,
            terrain: Tuple[float],
            x_scale: float,
            **kwargs,
            ):
        super().__init__(**kwargs)
        self.terrain = terrain
        self.x_scale = x_scale
        self.heightmapLoaded = False

    def episode_restart(self, bullet_client):
        self._p = bullet_client
        super().episode_restart(bullet_client)  # contains cpp_world.clean_everything()

        if not self.heightmapLoaded:
            self.heightmapLoaded = True
            # Choose a terrain
            terrain = self.terrain
            self.target_tile = len(terrain)

            # Build terrain: flat, gap, flat
            start = tuple((0.0 for _ in terrain))
            width = 100
            heightmap = np.array([list(start)+list(terrain)]*width).T
            self.terrain_id = add_heightmap_to_simulation(
                    self._p,
                    heightmap,
                    self.x_scale,
                    1.0,
                    1.0,
                )
            self.ground_plane_mjcf = [self.terrain_id]
            self.target_coords = [
                    self.target_tile*self.x_scale,
                    0,
                    0
                ]

            for i in self.ground_plane_mjcf:
                self._p.changeDynamics(i, -1, lateralFriction=0.8, restitution=0.5)
                self._p.changeVisualShape(i, -1, rgbaColor=[1, 1, 1, 0.8])
                self._p.configureDebugVisualizer(pybullet.COV_ENABLE_PLANAR_REFLECTION,i)


class BulletTerrainEnv(
        pybullet_envs.gym_locomotion_envs.WalkerBaseBulletEnv,
        abc.ABC,
        ):
    """Based on the PyBullet Gym environment but with customization of the
    observations and addition of terrains. The task is to traverse the terrain
    in less than a set number of time steps (8000 by default).

    The terrain is given as a sequence of floating-point numbers that encode
    a 2D height-field, which is extruded to form a 3D terrain.

    Observation: a tuple of
        - joint positions and angular speeds (floats)
        - feet contacts (1.0 or 0.0)
        - robot body velocity
        - time variable (see e.g. https://arxiv.org/pdf/1712.00378.pdf)
        - (optional) relative heightmap of the terrain (obtained through ray
          tracing with the terrain, floats).

    We do not use the original observations because they include features that
    made training impossible. For instance, the absolute z-position makes the
    observations from two terrains that only change in height different,
    although from the point of view of the robot they should be identical.

    The original reward takes into account the velocity towards the target
    point, but this measure is not ideal for terrains with varying heights
    due to the presence of local minima. We therefore also customize the reward
    function to instead consider the position on the x-axis, which for our
    specific terrains is more adequate.

    The `x_scale` parameter controls the resolution of the terrain. The smaller
    this number is, the smaller distance that each height in the terrain
    list represents.
    """
    # Pybullet tests are done on 1000 steps
    maximum_steps: int = 2000
    success_dist: float = 2.0
    target_velocity: float = 3.0
    last_x_positions_max_len: int = 20
    reward_on_completion: float = 0.0
    lidar_n: int = 20
    lidar_x_gap: float = 0.1
    lidar_relative_height: float = 1.0
    lidar_x_offset: float = 0.5
    minimum_relative_height = -5.0
    vx_weight: float = 2.0
    x_scale: float = 0.3
    allowable_foot_collisions: List[str] = []

    @staticmethod
    @abc.abstractmethod
    def robot_factory():
        pass

    def __init__(
            self,
            render: bool = False,
            terrain: Tuple[float] = None,
            ):
        robot = self.robot_factory()
        super().__init__(robot, render=render)

        if terrain is not None:
            self.terrain = terrain
        else:
            self.terrain = tuple([0]*200)
        self.fps = self.metadata["video.frames_per_second"]
        self.previous_vxs = deque(maxlen=self.last_x_positions_max_len)
        self.last_x_positions = deque(maxlen=self.last_x_positions_max_len)
        self.mean_vx = RunningMean(0.0)

        observation_dim = len(self.reset())
        high = np.ones([observation_dim])
        self.observation_space = gym.spaces.Box(-high, high)
        self.timesteps = 0
        self.stateId = -1

    @property
    def steps_remaining(self) -> int:
        """Number of steps remaining before episode ends."""
        return self.maximum_steps - self.timesteps

    @property
    def state(self):
        """Observation for agents."""
        # TODO: return [x, y, z, joint information...]
        raise NotImplementedError("Not yet implemented")

    @state.setter
    def state(self, new_state):
        # Set x, y, z and the joint information
        raise NotImplementedError("Not yet implemented")

    @property
    def current_absolute_coordinates(self) -> Tuple[float, float, float]:
        """Return current absolute coordinates of the robot."""
        x, y, z = self.robot.body_xyz
        return (x, y, z)

    @property
    def current_ground_z(self) -> float:
        """Return the height of the terrain at the coordinate of the robot."""
        x, _, z = self.current_absolute_coordinates
        i1 = int(x//self.x_scale)
        i1 = min(len(self.terrain)-1, max(0, i1))
        i2 = i1+1
        i2 = min(len(self.terrain)-1, max(0, i2))
        h1 = self.terrain[i1]
        h2 = self.terrain[i2]
        i1_real = min(len(self.terrain)-1, max(0, x/self.x_scale))
        c = i1_real - i1
        ground_z = (1-c)*h1 + c*h2
        return ground_z

    @property
    def relative_height(self) -> float:
        """Return the height of the robot relative to the ground."""
        _, _, z = self.current_absolute_coordinates
        return z - self.current_ground_z

    def create_single_player_scene(self, bullet_client):
        # We keep the "stadium" variable name to minimize amount of
        # code that needs to be modified from the HalfCheetah class
        self.stadium_scene = HeightmapScene(
                terrain=self.terrain,
                x_scale=self.x_scale,
                bullet_client=bullet_client,
                gravity=9.8*1.0,   # original: 9.8
                timestep=0.0165 / 4,
                frame_skip=4)
        return self.stadium_scene

    def absolute_terrain_height_at(self, x: float) -> float:
        current_index = int(x/self.x_scale)
        current_index = max(0, current_index)
        current_index = min(len(self.terrain)-1, current_index)
        lo_height = self.terrain[current_index]
        if current_index == len(self.terrain)-1:
            hi_height = lo_height
        else:
            hi_height = self.terrain[current_index+1]
        alpha = x/self.x_scale - int(x/self.x_scale)
        height = alpha*hi_height + (1-alpha)*lo_height
        return height

    @property
    def current_lidar(self):
        """Return the LIDAR observation at the current timestep."""
        x, y, z = self.current_absolute_coordinates
        to_x_gap = self.lidar_x_gap
        to_x_offset = self.lidar_x_offset
        ray_from_positions = [
                to_x_offset+x+to_x_gap*i
                for i in range(self.lidar_n)
            ]
        local_heightmap = [
            self.absolute_terrain_height_at(x)
            for x in ray_from_positions
        ]
        local_heightmap = [
            h-local_heightmap[0] for h in local_heightmap
        ]
        return np.array(local_heightmap)

    @property
    def current_distance_to_target(self):
        """Return the distance to the end of the terrain in the current
        timestep."""
        # Ignore z coordinate
        coords = self.current_absolute_coordinates[:-1]
        tcoords = self.stadium_scene.target_coords[:-1]
        distance = sum(((a-b)**2 for a, b in zip(coords, tcoords)))**(1/2)
        return distance

    def reset(self):
        """Reset the environment."""
        self.timesteps = 0

        if self.stateId >= 0:
            self._p.restoreState(self.stateId)
            return self.original_o

        # Reset robot
        with RedirectStream(sys.stdout):
             o = pybullet_envs.env_bases.MJCFBaseBulletEnv.reset(self)
        self._p.configureDebugVisualizer(pybullet.COV_ENABLE_RENDERING, 0)

        # Add terrain to scene
        self.parts, self.jdict, self.ordered_joints, self.robot_body = self.robot.addToScene(self._p, self.stadium_scene.ground_plane_mjcf)
        self.ground_ids = set([self.stadium_scene.terrain_id])
        self._p.configureDebugVisualizer(pybullet.COV_ENABLE_RENDERING, 1)

        # Save state without terrain if this is the first time
        if self.stateId < 0:
            self.stateId = self._p.saveState()

        # The original state does not include local heightmap
        o = np.concatenate([o, self.current_lidar])
        self.original_o = o
        return o

    @property
    def is_task_completed(self):
        return self.current_distance_to_target <= self.success_dist

    @property
    def is_out_of_bounds(self):
        x, y, z = self.current_absolute_coordinates
        x_out_of_bounds = abs(x) > len(self.terrain)*self.x_scale
        z_out_of_bounds = self.relative_height < self.minimum_relative_height
        return any((
                x_out_of_bounds,
                z_out_of_bounds,
            ))

    @property
    def is_done(self):
        termination_conditions = (
                self.steps_remaining <= 0,
                self.is_task_completed,
                #self.is_out_of_bounds,
            )
        return any(termination_conditions)

    def step(self, a):
        self.timesteps += 1

        # Feet contact is not updated appropriately in the super class, at
        # least not for the way terrain is implemented in this class. The
        # contact_ids set should only consist of the "body B" unique IDs,
        # instead of (bodyB ID, bodyB link) tuples, as in the super class.
        #
        # Original implementation:
        # https://github.com/bulletphysics/bullet3/blob/93be7e644024e92df13b454a4a0b0fcd02b21b10/examples/pybullet/gym/pybullet_envs/gym_locomotion_envs.py#L85
        feet_collision_cost = 0.0
        for i, f in enumerate(self.robot.feet):
            contact_ids = set(x[2] for x in f.contact_list())
            if (self.ground_ids & contact_ids):
                self.robot.feet_contact[i] = 1.0
                contact_foot = self.robot.foot_list[i]
                if contact_foot not in self.allowable_foot_collisions:
                    #feet_collision_cost += self.foot_collision_cost
                    pass
            else:
                self.robot.feet_contact[i] = 0.0

        o, r, d, info = super().step(a)

        # The original reward does not take into account foot collisions
        # https://github.com/bulletphysics/bullet3/blob/0e124cb2f103c40de4afac6c100b7e8e1f5d9e15/examples/pybullet/gym/pybullet_envs/gym_locomotion_envs.py#L92
        r += feet_collision_cost

        # The original state does not account for varying ground heights
        o[0] = o[0] - self.current_ground_z

        # The original state does not include local heightmap
        o = np.concatenate([o, self.current_lidar])

        #if self.is_done: d = True
        d = self.is_done
        if d: print(self.current_absolute_coordinates)
        if self.is_task_completed and d: r += self.reward_on_completion
        return o, r, d, info


class LabeledBulletTerrainEnv(BulletTerrainEnv):
    """Similar to the terrain environment, except that each height has a label.
    The label of the current height is encoded into the `info` dictionary and
    returned after each call to `step()`."""
    heightmap_tiles_n = 5
    def __init__(
            self,
            task: Tuple[Tuple[float, int], ...] = None,
            *args,
            **kwargs,
            ):
        if task is not None:
            task_annotations = [annotation for _, annotation in task]
            terrain = [height for height, _ in task]
        else:
            terrain = tuple([0]*200)
            task_annotations = [0 for _ in terrain]
        self.task_annotations = task_annotations
        super().__init__(*args, **kwargs,  terrain=terrain)

    def get_local_heightmap(self):
        x, y, z = self.current_absolute_coordinates
        x_scale = self.x_scale
        current_index = int(x/x_scale)
        current_index = max(0, current_index)
        current_index = min(len(self.task_annotations)-1, current_index)
        local_heightmap = self.task_annotations[current_index:current_index+self.heightmap_tiles_n]
        return local_heightmap

    def step(self, a):
        observation, reward, done, info = super().step(a)

        # Add terrain type to the information dictionary
        info["oracle"] = self.get_local_heightmap()

        return observation, reward, done, info


class HalfCheetahLabeledBulletTerrainEnv(LabeledBulletTerrainEnv):
    allowable_foot_collisions = ["ffoot", "bfoot"]

    @staticmethod
    def robot_factory():
        return pybullet_envs.robot_locomotors.HalfCheetah()


class HopperLabeledBulletTerrainEnv(LabeledBulletTerrainEnv):
    @staticmethod
    def robot_factory():
        return pybullet_envs.robot_locomotors.Hopper()


class AntLabeledBulletTerrainEnv(LabeledBulletTerrainEnv):
    @staticmethod
    def robot_factory():
        return pybullet_envs.robot_locomotors.Ant()
