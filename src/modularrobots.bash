#!/bin/bash
#SBATCH -J modularrobots
#SBATCH -o modularrobots_%j.out
#SBATCH -e modularrobots_%j.err
#SBATCH --mail-user=leohc@mit.edu
#SBATCH --mail-type=ALL
#SBATCH --gres=gpu:0
#SBATCH --nodes=1
#SBATCH --cpus-per-task=80
#SBATCH --mem=64G
#SBATCH --time=12:00:00

# User python environment
HOME2=/nobackup/users/$(whoami)
PYTHON_VIRTUAL_ENVIRONMENT=modularrobots
CONDA_ROOT=$HOME2/anaconda3

# Activate WMLCE virtual environment
source ${CONDA_ROOT}/etc/profile.d/conda.sh
conda activate $PYTHON_VIRTUAL_ENVIRONMENT
ulimit -s unlimited

# Execute script
time python main.py --hyperparameters_jsons hyperparameters/HalfCheetah.json
