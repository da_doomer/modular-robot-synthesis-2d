"""Defines tasks for environments."""
# Native imports
from typing import Collection
from collections.abc import Sequence as SequenceABC
from dataclasses import dataclass
from enum import IntEnum
from enum import auto
import random
from functools import reduce
from typing import TypeVar


Task = TypeVar("Task")


@dataclass
class Tasks(SequenceABC):
    train: Collection[Task]
    test: Collection[Task]

    @property
    def sequence(self):
        return tuple(self.train + self.test)

    def __iter__(self):
        return iter(self.sequence)

    def __len__(self):
        return len(self.sequence)

    def __contains__(self, thing):
        return thing in self.sequence

    def __getitem__(self, i: int):
        return self.sequence[i]


class Terrains(IntEnum):
    FLAT = auto()
    UP = auto()
    HURDLES = auto()
    STAIRS = auto()
    GAPS = auto()
    DOWN = auto()


def get_tasks(environment: str) -> Tasks:
    if "TerrainEnv" in environment:
        # Basic terrains
        def annotate_heights(heights, annotation):
            return tuple((h, annotation) for h in heights)

        terrain_len = 100
        flat = annotate_heights((0,)*terrain_len, Terrains.FLAT)

        def slope_creator(slope):
            return (0,) + tuple(slope*i for i in range(terrain_len))

        up = annotate_heights(slope_creator(0.06), Terrains.UP)
        down = annotate_heights(slope_creator(-0.06), Terrains.DOWN)
        hurdles = annotate_heights(
                ((0, 0, 0, 0, 0, 0, 0, 0.2, 0.2)*terrain_len)[:terrain_len],
                Terrains.HURDLES,
            )

        # Concatenate terrains
        def extended_terrain(terrain, section):
            new_terrain = list(terrain)
            for height, label in section:
                new_terrain.append((terrain[-1][0] + height, label))
            return tuple(new_terrain)

        step = annotate_heights((0,)*4 + (0.2,)*4, Terrains.STAIRS)
        stairs = reduce(
                extended_terrain,
                [step for _ in range(terrain_len)],
            )[:terrain_len]

        gaps = annotate_heights(
                (((0,)*4 + (0.1,)*4)*terrain_len)[:terrain_len],
                Terrains.GAPS,
            )

        _random = random.Random("generate some cool tasks!")

        def random_combined_terrain(sections, section_len, n):
            terrain = tuple()
            section = _random.choice(sections)
            for _ in range(n):
                if len(terrain) == 0:
                    terrain = section[:section_len]
                else:
                    terrain = extended_terrain(terrain, section[:section_len])
                next_section = _random.choice(sections)
                while next_section == section and len(sections) > 1:
                    next_section = _random.choice(sections)
                section = next_section
            return terrain

        # Build tasks
        section_len = 40
        section_n = 6
        terrain_n = 10
        train_tasks = [flat, up, hurdles, stairs, gaps, down]
        #train_tasks = [flat, up]
        #train_tasks = [flat]
        test_tasks = [
                random_combined_terrain(
                    train_tasks,
                    section_len,
                    section_n,
                )
                for _ in range(terrain_n)
            ]
        return Tasks(train_tasks, test_tasks)
    raise ValueError(f"Terrains not defined for {environment}")
