"""Multiplexers."""
# Native imports
from typing import Dict
from typing import List
from collections import defaultdict
import concurrent.futures
from itertools import product
from pathlib import Path
import os

# Local imports
from policies import Policy
from policies import SafetyAwarePolicy
from policies import PolicyContainer
from worldmodels import WorldModel
from worldmodels import WorldModelFactory
from worldmodels import WorldModelFiniteHorizonEnv
import optimization

# External imports
import gym
from stable_baselines3 import PPO
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.vec_env import SubprocVecEnv
from stable_baselines3.common.vec_env import VecNormalize
from stable_baselines3.common.monitor import Monitor
import torch.nn


def get_future_actions(
        policy: Policy,
        timesteps: int,
        starting_state,
        observation,
        info,
        environment: str,
        env_kwargs: dict,
        ) -> float:
    env = gym.make(environment, **env_kwargs)
    env.reset()
    world_model = WorldModelFactory(
            environment,
        )
    world_model._env = env
    world_model.state = starting_state
    _env = env
    _obs = observation
    _info = info
    old_x, _, _ = env.robot.body_xyz
    actions = list()
    for _ in range(timesteps):
        action = policy(_env, _obs, _info)
        _env, _obs, reward, done, _info = world_model.predict(
                _env, _obs, _info, policy
            )
        actions.append(action)
        if done:
            break
    env.close()
    return actions


def evaluate_policy_sequence(
        policy_sequence: List[Policy],
        starting_state,
        observation,
        info,
        environment: str,
        env_kwargs: dict,
        ) -> float:
    env = gym.make(environment, **env_kwargs)
    env.reset()
    world_model = WorldModelFactory(
            environment,
        )
    world_model._env = env
    world_model.state = starting_state
    _env = env
    _obs = observation
    _info = info
    old_x, _, _ = env.robot.body_xyz
    total_reward = 0.0
    for i, policy in enumerate(policy_sequence):
        action = policy(_env, _obs, _info)
        _env, _obs, reward, done, _info = world_model.predict(
                _env, _obs, _info, policy
            )
        total_reward += reward
        if done:
            break
    env.close()
    return total_reward


class AbruptMultiplexer(Policy):
    """Active the policies according to the `info["oracle"]` integer
    given by the environment.

    That is, this module defines the following function

    multiplexer(s_t, oracle_i) = policies[i](s_t)
    """
    def __init__(
            self,
            policies: Dict[int, Policy],
            ):
        super().__init__()
        self.policies = policies
        self.timestep = 0

    def get_policy_i(self, info):
        """Extracts the policy index from the given `info` dictionary given by
        the environment."""
        if "oracle" in info.keys():
            heightmap = info["oracle"]
            i = heightmap[0]
            if i in self.policies.keys():
                return i
        return list(self.policies.keys())[0]

    def get_future_policy_i(self, info):
        """Extracts the policy index from the given `info` dictionary given by
        the environment."""
        if "oracle" in info.keys():
            heightmap = info["oracle"]
            i = heightmap[-1]
            if i in self.policies.keys():
                return i
        return list(self.policies.keys())[0]

    def __enter__(self):
        super().__enter__()
        self.timestep = 0

    def __call__(self, env, obs, info):
        # Assume the environment tells us which policy we should use
        # at every time-step.
        current_policy_i = self.get_policy_i(info)
        policy = self.policies[current_policy_i]
        executed_policy_name = f"{current_policy_i.name}"
        action = policy(env, obs, info)
        self.log("observation", obs)
        self.log("action", action)
        self.log("current_policy_i", executed_policy_name)
        self.timestep += 1
        print(f"{self.timestep}, {env.unwrapped.robot.body_xyz}")
        return action


class PlanningMultiplexer(AbruptMultiplexer):
    """Activate the policies according to the `info["oracle"]` integer
    given by the environment, but plan the actions when a transition is
    detected over the horizon length.

    That is, when a transition is not detected in the `horizon_len`, this
    module defines the following function:

    multiplexer(s_t, oracle_i) = policies[i](s_t)

    When a transition is detected, this module approximately solves for
    the following action sequence:

    a_t, ..., a_{t+horizon_len*2}
    = min[a = a_t, ..., a_{t+horizon_len*2}] reward(a)

    This equation is approximately solved through CMA-ES, which is seeded with
    actions sampled from policies[source] and policies[target]. Specifically,
    CMA-ES is seeded with

    a_[i] = policies[source](s_i)

    for t<=i<=t+horizon_len; where s_i is computed with policies[source] under
    forward simulation. And similarly

    a_[i] = policies[target](s_i)

    for t<=i<=t+horizon_len*2; where s_i is computed with policies[target]
    under forward simulation.
    """
    def __init__(
            self,
            policies: Dict[int, Policy],
            world_model: WorldModel,
            horizon_len: int,
            action_size: int,
            max_rollouts: int,
            trajectory_optimization_sigma: float,
            environment: str,
            env_kwargs: dict,
            executor: concurrent.futures.Executor,
            ):
        super().__init__(policies=policies)
        self.policies = policies
        self.horizon_len = horizon_len
        self.action_size = action_size
        self.world_model = world_model
        self.max_rollouts = max_rollouts
        self.plan = list()
        self.environment = environment
        self.env_kwargs = env_kwargs
        self.timestep = 0
        self.executor = executor
        self.trajectory_optimization_sigma = trajectory_optimization_sigma

    def __enter__(self):
        super().__enter__()
        self.plan = list()

    @property
    def abrupt_multiplexer(self) -> AbruptMultiplexer:
        """An `AbruptMultiplexer` with the same primitive policies."""
        return AbruptMultiplexer(self.policies)

    def get_plan(
            self,
            observation,
            info: dict,
            ):
        """Return an action sequence for the given observation and the given
        seeder policy. The seeder policy will commonly be an
        `AbruptMultiplexer` used to get an initial guess for a policy
        transition."""
        # Use the current policy to get the starting guess
        # TODO verify the starting state a function argument to getplan()
        previous_trajectory = get_future_actions(
                    policy=self.abrupt_multiplexer,
                    timesteps=self.horizon_len*2,
                    starting_state=self.world_model.state,
                    observation=observation,
                    info=info,
                    environment=self.environment,
                    env_kwargs=self.env_kwargs,
                )
        plan = optimization.cma_receding_horizon(
                horizon_len=self.horizon_len*2,
                action_size=self.action_size,
                starting_state=self.world_model.state,
                max_rollouts=self.max_rollouts,
                environment=self.environment,
                env_kwargs=self.env_kwargs,
                executor=self.executor,
                observation=observation,
                previous_trajectory=previous_trajectory,
                sigma=self.trajectory_optimization_sigma,
            )
        return plan

    def __call__(self, env, obs, info):
        # Assume the environment tells us which policy we should use
        # at every time-step.
        current_policy_i = self.get_policy_i(info)
        policy = self.policies[current_policy_i]

        # If we had a plan, we execute it until completion.
        # Otherwise, if we don't switch policies, or we will reach a safe state
        # by simply executing our policy, there is nothing for us to do.
        # Otherwise, we know we will not reach a safe state to transition.
        # We need to search for a plan to arrive at a state that is
        # safe for the other policy. The plan has twice the horizon length
        # of the policy-change simulation, so that we have time to "stabilize"
        # the robot for the next policy.
        if len(self.plan) > 0:
            action = self.plan.pop(0)
            executed_policy_name = "PLAN"
        else:
            # Use the world model to execute the current policy and find out
            # which policy we should be running in the near-future.
            self.world_model._env = env.unwrapped
            state = self.world_model.state
            _env, _obs, _info = env, obs, info
            for _ in range(self.horizon_len):
                _env, _obs, _reward, _done, _info = self.world_model.predict(
                        _env, _obs, _info, policy
                    )
                if _done:
                    break
            self.world_model.state = state
            future_policy_i = self.get_policy_i(_info)

            # If we predict a change of policy, plan the transition. Otherwise,
            # use the current policy.
            if future_policy_i == current_policy_i:
                action = self.policies[future_policy_i](env, obs, info)
                executed_policy_name = future_policy_i.name
            else:
                self.world_model._env = env.unwrapped
                self.plan = self.get_plan(
                        observation=obs,
                        info=info,
                    )
                action = self.plan.pop(0)
                executed_policy_name = "PLAN"

        self.log("observation", obs)
        self.log("action", action)
        self.log("current_policy_i", executed_policy_name)
        # TODO remove debugging output
        self.timestep += 1
        print(f"{self.timestep}, {env.unwrapped.robot.body_xyz}")
        return action


class SafetyAwareMultiplexer(PlanningMultiplexer):
    """A planning policy multiplexer which optimizes plans for transitions
    between primitive policies for safety of the final state under the target
    policy."""
    def get_plan(
            self,
            observation,
            safety_predictor,
            info,
            ):
        # Use the current policy to get the starting guess
        previous_trajectory = get_future_actions(
                    policy=self.abrupt_multiplexer,
                    timesteps=self.horizon_len*2,
                    starting_state=self.world_model.state,
                    observation=observation,
                    info=info,
                    environment=self.environment,
                    env_kwargs=self.env_kwargs,
                )
        plan = optimization.cma_receding_horizon(
                horizon_len=self.horizon_len*2,
                action_size=self.action_size,
                starting_state=self.world_model.state,
                max_rollouts=self.max_rollouts,
                environment=self.environment,
                env_kwargs=self.env_kwargs,
                executor=self.executor,
                observation=observation,
                previous_trajectory=previous_trajectory,
                safety_predictor=safety_predictor,
                sigma=self.trajectory_optimization_sigma,
            )
        return plan

    def __call__(self, env, obs, info):
        # Assume the environment tells us which policy we should use
        # at every time-step.
        current_policy_i = self.get_policy_i(info)
        policy = self.policies[current_policy_i]
        future_safety = 0.0

        # If we had a plan, we execute it until completion.
        # Otherwise, if we don't switch policies, or we will reach a safe state
        # by simply executing our policy, there is nothing for us to do.
        # Otherwise, we know we will not reach a safe state to transition.
        # We need to search for a plan to arrive at a state that is
        # safe for the other policy. The plan has twice the horizon length
        # of the policy-change simulation, so that we have time to "stabilize"
        # the robot for the next policy.
        if len(self.plan) > 0:
            action = self.plan.pop(0)
            executed_policy_name = "PLAN"
        else:
            # Use the world model to execute the current policy and find out
            # which policy we should be running in the near-future.
            self.world_model._env = env.unwrapped
            state = self.world_model.state
            _env, _obs, _info = env, obs, info
            for _ in range(self.horizon_len):
                _env, _obs, _reward, _done, _info = self.world_model.predict(
                        _env, _obs, _info, policy
                    )
                if _done or self.get_policy_i(_info) != current_policy_i:
                    break
            self.world_model.state = state
            future_policy_i = self.get_policy_i(_info)

            if future_policy_i == current_policy_i:
                action = self.policies[current_policy_i](env, obs, info)
                executed_policy_name = f"{current_policy_i.name}"
            else:
                future_policy = self.policies[future_policy_i]
                self.world_model._env = env.unwrapped
                self.plan = self.get_plan(
                        observation=obs,
                        safety_predictor=future_policy.safety_predictor,
                        info=info,
                    )
                action = self.plan.pop(0)
                executed_policy_name = "PLAN"

        self.log("observation", obs)
        self.log("action", action)
        self.log("future_safety", future_safety)
        self.log("current_policy_i", executed_policy_name)
        self.timestep += 1
        # TODO remove debugging output
        if self.timestep % 100 == 1:
            print(f"{self.timestep}, pos: {env.unwrapped.robot.body_xyz}, future_safety: {future_safety}")
        return action


class FutureVerifyingMultiplexer(PlanningMultiplexer):
    """A planning policy multiplexer which considers liveness of the final
    state under the target policy when optimizing a transition."""
    def get_plan(
            self,
            observation,
            target_policy_i,
            info,
            starting_state,
            ):
        # Use the current policy to get the starting guess
        previous_trajectory = get_future_actions(
                   policy=self.abrupt_multiplexer,
                   timesteps=self.horizon_len*2,
                   starting_state=self.world_model.state,
                   observation=observation,
                   info=info,
                   environment=self.environment,
                   env_kwargs=self.env_kwargs,
               )
        plan = optimization.cma_receding_horizon(
                horizon_len=self.horizon_len*2,
                action_size=self.action_size,
                starting_state=starting_state,
                max_rollouts=self.max_rollouts,
                environment=self.environment,
                env_kwargs=self.env_kwargs,
                executor=self.executor,
                observation=observation,
                previous_trajectory=previous_trajectory,
                target_policy=self.policies[target_policy_i],
                target_policy_timesteps=self.horizon_len*2,
                sigma=self.trajectory_optimization_sigma,
            )
        return plan

    def __call__(self, env, obs, info):
        # Assume the environment tells us which policy we should use
        # at every time-step.
        current_policy_i = self.get_policy_i(info)
        policy = self.policies[current_policy_i]

        # If we had a plan, we execute it until completion.
        # Otherwise, if we don't switch policies, or we will reach a safe state
        # by simply executing our policy, there is nothing for us to do.
        # Otherwise, we know we will not reach a safe state to transition.
        # We need to search for a plan to arrive at a state that is
        # safe for the other policy. The plan has twice the horizon length
        # of the policy-change simulation, so that we have time to "stabilize"
        # the robot for the next policy.
        if len(self.plan) > 0:
            action = self.plan.pop(0)
            executed_policy_name = "PLAN"
        else:
            # Use the world model to execute the current policy and find out
            # which policy we should be running in the near-future.
            future_policy_i = self.get_future_policy_i(info)

            # If we predict a change of policy, plan the transition. Otherwise,
            # use the current policy.
            #if future_policy_i == current_policy_i or self.timestep == 0:
            #if future_policy_i == current_policy_i or self.timestep == 0:
            if "oracle" not in info.keys() or len(set(info["oracle"])) <= 1:
                action = self.policies[future_policy_i](env, obs, info)
                executed_policy_name = future_policy_i.name
            else:
                self.world_model._env = env.unwrapped
                state = self.world_model.state
                self.plan = self.get_plan(
                        target_policy_i=future_policy_i,
                        observation=obs,
                        info=info,
                        starting_state=state,
                    )
                action = self.plan.pop(0)
                executed_policy_name = "PLAN"

        self.log("observation", obs)
        self.log("action", action)
        self.log("current_policy_i", executed_policy_name)
        # TODO remove debugging output
        self.timestep += 1
        print(f"{self.timestep}, {env.unwrapped.robot.body_xyz}, {executed_policy_name}")
        return action


def make_env(env_id: str, *args, **kwargs):
    def _init():
        env = Monitor(gym.make(env_id, *args, **kwargs))
        return env
    return _init


def make_finite_horizon_env(
        environment: str,
        initial_state,
        initial_observation,
        horizon_len: int,
        finish_policy: Policy = None,
        finish_policy_timesteps: int = 0,
        ):
    def _init():
        return WorldModelFiniteHorizonEnv(
                initial_state=initial_state,
                initial_observation=initial_observation,
                environment=environment,
                horizon_len=horizon_len,
                finish_policy=finish_policy,
                finish_policy_timesteps=finish_policy_timesteps,
            )
    return _init


class TransitionTrainerMultiplexer(AbruptMultiplexer):
    """A multiplexer which optimizes a transition policy for each
    source-target policy pair.

    Primitive policies are executed until a transition is detected in the
    planning horizon. At that point the corresponding source-target policy is
    trained with an RL algorithm over an environment defined as follows:

        - s_0: the state at which the transition was detected
        - horizon_len*2: maximum timesteps
        - transition function is defined by forward simulation

    The RL hyperparameters are given as a dictionary `policy_hyperparameters`.
    """
    def __init__(
            self,
            policies: Dict[int, Policy],
            world_model: WorldModel,
            horizon_len: int,
            action_size: int,
            transition_training_timesteps: int,
            environment: str,
            env_kwargs: dict,
            executor: concurrent.futures.Executor,
            policy_hyperparameters: dict,
            log_dir: Path,
            policy_optimization_workers: int,
            finish_policy_timesteps: int,
            ):
        super().__init__(
                policies=policies,
                )
        self.world_model = world_model
        self.horizon_len = horizon_len
        self.action_size = action_size
        self.transition_training_timesteps = transition_training_timesteps
        self.environment = environment
        self.env_kwargs = env_kwargs
        self.executor = executor
        self.plan = list()
        self.timestep = 0
        self.executor = executor
        self.policy_optimization_workers = policy_optimization_workers
        self.finish_policy_timesteps = finish_policy_timesteps

        self.training = True
        self.executed_policy = None

        # Instantiate the transition policies
        self.transition_policies = defaultdict(dict)
        num_envs = os.cpu_count()
        for i, j in product(policies.keys(), policies.keys()):
            if i == j:
                continue
            # The code below is partially present in algorithms.py.
            # Merge the two in a single function make_policy(...)?
            envs = [make_env(environment) for _ in range(num_envs)]
            vecenv = DummyVecEnv(envs)
            vecenv = VecNormalize(
                    vecenv,
                    norm_obs=True,
                    norm_reward=True,
                    clip_obs=10.0,
                )
            if "policy_kwargs" in policy_hyperparameters.keys()\
                    and isinstance(policy_hyperparameters["policy_kwargs"], str):
                policy_hyperparameters["policy_kwargs"] = eval(
                        policy_hyperparameters["policy_kwargs"]
                    )
            # Use random weights as a starting point
            # In general, using the parameters of the source and
            # target policies could offer performance boosts in some settings,
            # but this is problem-dependent. For example, in some problems
            # the distribution of observations and actions in the transitions
            # is similar enough so that the aforementioned parameters can be
            # used as a starting point. In this case, in the name of simplicity,
            # we use random parameters as a starting point.
            policy = PPO(
                    'MlpPolicy',
                    vecenv,
                    verbose=1,
                    **policy_hyperparameters,
	    )
            policy_id = f"{str(i)}_{str(j)}"
            policy_path = log_dir/f"{policy_id}.zip"
            vecnorm_path = log_dir/f"{policy_id}_vecnorm.pkl"
            policy.save(policy_path)
            vecenv.save(vecnorm_path)
            container = PolicyContainer(policy_path, vecnorm_path, environment)
            self.transition_policies[i][j] = container

    def __enter__(self):
        super().__enter__()
        self.plan = list()

    def train_transition(
            self,
            observation,
            source_policy_i,
            target_policy_i,
            info,
            starting_state,
            ):
        i = source_policy_i
        j = target_policy_i

        # Build a vectorized environment
        # TODO parametrize finish policy timesteps
        envs = [make_finite_horizon_env(
            environment=self.environment,
            initial_state=starting_state,
            initial_observation=observation,
            horizon_len=self.horizon_len*2,
            finish_policy=self.policies[j],
            finish_policy_timesteps=self.finish_policy_timesteps,
            ) for _ in range(self.policy_optimization_workers)]
        vecenv = DummyVecEnv(envs)
        vecenv.reset()

        # Reconstruct the corresponding policy
        container = self.transition_policies[i][j]
        policy = container.policy
        vecnorm = VecNormalize.load(str(container.vecnorm_path), vecenv)
        vecnorm.reset()
        policy.set_env(vecnorm)

        # Iterate the optimization algorithm
        timesteps = self.transition_training_timesteps
        print(f"Training for {timesteps}")
        policy.learn(timesteps, reset_num_timesteps=False)

        # Update versions to disk
        vecnorm.close()
        policy.save(container.policy_path)
        vecnorm.save(container.vecnorm_path)

    def get_plan(
            self,
            observation,
            source_policy_i,
            target_policy_i,
            info,
            starting_state,
            ):
        if self.training:
            self.train_transition(
                    source_policy_i=source_policy_i,
                    target_policy_i=target_policy_i,
                    observation=observation,
                    info=info,
                    starting_state=starting_state,
                )

        # Return the output of the corresponding transition policy
        i = source_policy_i
        j = target_policy_i
        transition_policy = self.transition_policies[i][j]
        plan = [transition_policy for _ in range(self.horizon_len*2)]
        return plan

    def __call__(self, env, obs, info):
        # Assume the environment tells us which policy we should use
        # at every time-step.
        current_policy_i = self.get_policy_i(info)
        self.world_model._env = env.unwrapped

        # If we had a plan, we execute it until completion.
        # Otherwise, if we don't switch policies, or we will reach a safe state
        # by simply executing our policy, there is nothing for us to do.
        # Otherwise, we know we will not reach a safe state to transition.
        # We need to search for a plan to arrive at a state that is
        # safe for the other policy. The plan has twice the horizon length
        # of the policy-change simulation, so that we have time to "stabilize"
        # the robot for the next policy.
        if len(self.plan) > 0:
            policy = self.plan.pop(0)
            action = policy(env, obs, info)
        else:
            # Use the world model to execute the current policy and find out
            # which policy we should be running in the near-future.
            state = self.world_model.state
            policy = self.policies[current_policy_i]
            _env, _obs, _info = env, obs, info
            for _ in range(self.horizon_len):
                _env, _obs, _reward, _done, _info = self.world_model.predict(
                        _env, _obs, _info, policy
                    )
                if _done or self.get_policy_i(_info) != current_policy_i:
                    break
            future_policy_i = self.get_policy_i(_info)

            # If we predict a change of policy, plan the transition. Otherwise,
            # use the current policy.
            if future_policy_i == current_policy_i or self.timestep == 0:
                action = self.policies[future_policy_i](env, obs, info)
                self.executed_policy = future_policy_i.name
            else:
                self.plan = self.get_plan(
                        source_policy_i=current_policy_i,
                        target_policy_i=future_policy_i,
                        observation=obs,
                        info=info,
                        starting_state=state,
                    )
                policy = self.plan.pop(0)
                action = policy(env, obs, info)
                self.executed_policy = f"({current_policy_i.name}, {future_policy_i.name})"
            self.world_model.state = state

        self.log("observation", obs)
        self.log("action", action)
        self.log("current_policy_i", str(self.executed_policy))
        # TODO remove debugging output
        self.timestep += 1
        self.log("vx", env.unwrapped.current_vx)
        print(f"{self.timestep}, {env.unwrapped.robot.body_xyz}, {self.executed_policy}")
        return action


class AbruptPlanningMultiplexer(AbruptMultiplexer):
    """Optimize the abrupt transition of policies. Specifically, look-ahead
    `horizon_len` steps.

    If an `info["oracle"]` integer change occurs, search for the optimal
    transition change in a timeframe of `2*horizon_len` steps.

    Otherwise, use the policy specified by `info["oracle"]`.
    """
    def __init__(
            self,
            policies: Dict[int, Policy],
            world_model: WorldModel,
            horizon_len: int,
            environment: str,
            env_kwargs: dict,
            executor: concurrent.futures.Executor,
            ):
        super().__init__(policies=policies)
        self.policies = policies
        self.horizon_len = horizon_len
        self.world_model = world_model
        self.plan = list()
        self.environment = environment
        self.env_kwargs = env_kwargs
        self.timestep = 0
        self.executor = executor

    def __enter__(self):
        super().__enter__()
        self.plan = list()

    def __call__(self, env, obs, info):
        # Assume the environment tells us which policy we should use
        # at every time-step.
        current_policy_i = self.get_policy_i(info)
        policy = self.policies[current_policy_i]

        # If we had a plan, we execute it until completion.
        # Otherwise, if we don't switch policies, or we will reach a safe state
        # by simply executing our policy, there is nothing for us to do.
        # Otherwise, we know we will not reach a safe state to transition.
        # We need to search for a plan to arrive at a state that is
        # safe for the other policy. The plan has twice the horizon length
        # of the policy-change simulation, so that we have time to "stabilize"
        # the robot for the next policy.
        if len(self.plan) > 0:
            policy_i = self.plan.pop(0)
            action = self.policies[policy_i](env, obs, info)
            executed_policy_name = policy_i.name
        else:
            future_policy_i = self.get_future_policy_i(info)

            # If we predict a change of policy, plan the transition. Otherwise,
            # use the current policy.
            if "oracle" not in info.keys() or len(set(info["oracle"])) <= 1:
                action = self.policies[current_policy_i](env, obs, info)
                executed_policy_name = current_policy_i.name
            else:
                self.world_model._env = env.unwrapped
                state = self.world_model.state
                # Evaluate all policy sequences and choose the best one
                plan_len = self.horizon_len*2
                plans = []
                for i in range(plan_len):
                    plan = [current_policy_i]*i + [future_policy_i]*(plan_len-i)
                    plans.append(plan)
                futures = list()
                for plan in plans:
                    policy_sequence = [self.policies[i] for i in plan]
                    future = self.executor.submit(
                        evaluate_policy_sequence,
                        policy_sequence=policy_sequence,
                        starting_state=state,
                        observation=obs,
                        info=info,
                        environment=self.environment,
                        env_kwargs=self.env_kwargs,
                    )
                    futures.append(future)
                plan_rewards = [future.result() for future in futures]
                best_plan = max(
                    zip(plans, plan_rewards),
                    key=lambda pr: pr[1]
                )[0]
                self.plan = best_plan
                policy_i = self.plan.pop(0)
                action = self.policies[policy_i](env, obs, info)
                executed_policy_name = policy_i.name

        self.log("observation", obs)
        self.log("action", action)
        self.log("current_policy_i", executed_policy_name)
        # TODO remove debugging output
        self.timestep += 1
        print(f"{self.timestep}, {env.unwrapped.robot.body_xyz}")
        return action


class TransitionPolicyMultiplexer(AbruptMultiplexer):
    """A planning policy multiplexer which considers liveness of the final
    state under the target policy when optimizing a transition."""
    def __init__(
            self,
            policies: Dict[int, Policy],
            transition_policy: Policy,
            horizon_len: int,
            ):
        super().__init__(policies=policies)
        self.policies = policies
        self.transition_policy = transition_policy
        self.plan = list()
        self.horizon_len = horizon_len
        self.timestep = 0

    def __enter__(self):
        super().__enter__()
        self.plan = list()

    def __call__(self, env, obs, info):
        # Assume the environment tells us which policy we should use
        # at every time-step.
        current_policy_i = self.get_policy_i(info)
        policy = self.policies[current_policy_i]

        # If we had a plan, we execute it until completion.
        # Otherwise, if we don't switch policies, or we will reach a safe state
        # by simply executing our policy, there is nothing for us to do.
        # Otherwise, we know we will not reach a safe state to transition.
        # We need to search for a plan to arrive at a state that is
        # safe for the other policy. The plan has twice the horizon length
        # of the policy-change simulation, so that we have time to "stabilize"
        # the robot for the next policy.
        if len(self.plan) > 0:
            policy = self.plan.pop(0)
            action = policy(env, obs, info)
            executed_policy_name = "TRANSITION"
        else:
            # Use the world model to execute the current policy and find out
            # which policy we should be running in the near-future.
            future_policy_i = self.get_future_policy_i(info)

            # If we predict a change of policy, plan the transition. Otherwise,
            # use the current policy.
            #if future_policy_i == current_policy_i or self.timestep == 0:
            #if future_policy_i == current_policy_i or self.timestep == 0:
            if "oracle" not in info.keys() or len(set(info["oracle"])) <= 1:
                action = self.policies[future_policy_i](env, obs, info)
                executed_policy_name = future_policy_i.name
            else:
                self.plan = [self.transition_policy]*self.horizon_len*2
                policy = self.plan.pop(0)
                action = policy(env, obs, info)
                executed_policy_name = "TRANSITION"

        self.log("observation", obs)
        self.log("action", action)
        self.log("current_policy_i", executed_policy_name)
        # TODO remove debugging output
        self.timestep += 1
        print(f"{self.timestep}, {env.unwrapped.robot.body_xyz}, {executed_policy_name}")
        return action
