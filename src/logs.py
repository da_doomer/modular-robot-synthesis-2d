"""Defines logging procedures."""
# Native imports
from pathlib import Path
import time
from typing import List
from typing import Dict
import json
import statistics
import csv
import concurrent.futures
import shutil
import traceback
from itertools import chain
import uuid

# External imports
import matplotlib
matplotlib.use('SVG')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import ffmpeg
from sklearn.preprocessing import StandardScaler as Scaler

# Local imports
import simulation
from policies import Policy
from tasks import Task
from tasks import get_tasks


def get_video_fps(video: Path) -> float:
    probe = ffmpeg.probe(str(video))
    video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    fps1, fps2 = map(float, video_stream['avg_frame_rate'].split("/"))
    return fps1/fps2


def stack_videos(
        video1: Path,
        video2: Path,
        output_video: Path,
        ):
    # Get video heights
    probe1 = ffmpeg.probe(str(video1))
    video_stream = next((stream for stream in probe1['streams'] if stream['codec_type'] == 'video'), None)
    h1 = int(video_stream['height'])
    w1 = int(video_stream['width'])
    probe2 = ffmpeg.probe(str(video2))
    video_stream = next((stream for stream in probe2['streams'] if stream['codec_type'] == 'video'), None)
    h2 = int(video_stream['height'])
    w2 = int(video_stream['width'])

    # Scale videos to maximum height and correct frame-rate
    h = max(h1, h2)
    w1 = w1 - (w1 % 2)
    w2 = w2 - (w2 % 2)
    in1 = ffmpeg.input(str(video1)).filter("crop", w=w1).filter('scale', w=w1, h=h)
    in2 = ffmpeg.input(str(video2)).filter("crop", w=w2).filter('scale', w=w2, h=h)

    # Stack videos horizontally
    ffmpeg.filter([in1, in2], 'hstack').output(str(output_video)).run()
    print(f"Wrote {output_video}")


def log_policy(
        policy: Policy,
        environment: str,
        log_dir: Path,
        max_rollout_len: int,
        executor: concurrent.futures.Executor,
        risk_estimator=None,
        animate: bool = True,
        seed: int = None,
        environment_kwargs=dict(),
        ):
    # Log policy
    print("Logging policy")
    id = int(time.time()*100)
    if animate:
        recording_dir = log_dir/f"{environment}_recording"
        recording_dir.mkdir()
        print("mkdir {}".format(recording_dir))
    else:
        recording_dir = None

    # I have no idea why PyBullet crashes sometimes. This is the error:
    #
    #      pybullet_envs/env_bases.py", line 169, in move_and_look_at
    #      camInfo = self.env._p.getDebugVisualizerCamera()
    #      pybullet.error: Not connected to physics server.
    #
    # It seems to be related with opening a lot of environments (it happens
    # after multi-threaded optimization with CMA, but not consistently).
    #
    # Possibly related issues:
    #
    #      https://github.com/bulletphysics/bullet3/issues/2470
    #      https://github.com/bulletphysics/bullet3/issues/2446
    #      https://github.com/bulletphysics/bullet3/pull/3819
    #
    # For now, a quick-n-dirty fix will suffice. We retry sampling rollouts
    # until one succeeds.
    success = False
    tries = 3
    for i in range(tries):
        if recording_dir is not None:
            shutil.rmtree(recording_dir, ignore_errors=True)
            recording_dir.mkdir(exist_ok=True)
        try:
            rewards, logs = simulation.simulate(
                    environment,
                    policy,
                    recording_dir=recording_dir,
                    seed=seed,
                    env_kwargs=environment_kwargs,
                    steps=max_rollout_len,
                    executor=executor,
                )
            success = True
        except Exception as e:
            if i == tries-1 or isinstance(e, KeyboardInterrupt):
                raise e
            print(f"Fatal exception while trying to plot:\n{e}\nRetrying.")
            traceback.print_exc()
        if success:
            break

    # Save raw logs
    for key, values in logs.items():
        json_file = log_dir/f"{environment}_{id}_{key}.json"
        with open(json_file, "wt") as f:
            try:
                json.dump(values, f, indent=2)
            except TypeError:
                # Numpy data structures cannot be serialized,
                # try to convert.
                json.dump(np.array(values).squeeze().tolist(), f, indent=2)
            print(f"Wrote {json_file}")

    # Save animations if required
    if animate:
        recording_animation = list(recording_dir.glob("*mp4"))[0]
        fps = get_video_fps(recording_animation)
        for key, values in logs.items():
            animation = log_dir/f"{environment}_{id}_{key}.mp4"
            if "plan" == key:
                animationf = array_animation
            elif "safety" == key:
                animationf = barchart_animation
            else:
                animationf = timeseries_animation
            success = animationf(
                data=values,
                filename=animation,
                fps=fps,
                executor=executor,
            )
            if success:
                joint_animation = log_dir/f"{environment}_{id}_{key}.concat.mp4"
                stack_videos(
                        recording_animation,
                        animation,
                        joint_animation
                    )
    return rewards


def _animate(i, data, filename, fps):
    # Find data statistics
    data = np.array(data).squeeze()
    if len(data.shape) < 2:
        data = np.expand_dims(data, -1)
    channel_mins = list()
    channel_maxs = list()
    for channel in data.T:
        channel_mins.append(min(channel))
        channel_maxs.append(max(channel))
    x = range(len(data))
    window_size = fps*5

    # Build plot figure
    timesteps, channels = data.shape
    px = 1.0/plt.rcParams['figure.dpi']
    fig, axs = plt.subplots(
            nrows=channels,
            figsize=(600*px, 400*px),
            squeeze=False,
            )
    axs = axs.flatten()
    fig.subplots_adjust(left=.50, right=.90, bottom=.02, top=.94, hspace=0.0)
    xstart = i-window_size
    xend = i
    for ax, channel, channel_min, channel_max in zip(
            axs, data.T, channel_mins, channel_maxs
            ):
        ax.clear()
        ax.set_xticks([])
        y = channel
        ax.plot(x, y)
        ax.set_xlim(left=xstart, right=xend)
    fig.savefig(filename)
    plt.close(fig)
    return True

def timeseries_animation(
        data: List[List[float]],  # time[channels[...]]
        filename: Path,
        fps: int,
        executor: concurrent.futures.Executor,
        ) -> bool:
    if len(data) <= 1:
        print("Skipping bar animation because len(data) <= 1")
        return False

    # Plot all images in parallel
    futures = list()
    anim_id = str(uuid.uuid4())
    filenames = list()
    for i in range(len(data)):
        img_id = f"{i}".rjust(len(str(len(data))))
        figfilename = (filename.parent/(anim_id+img_id)).with_suffix(f".png")
        filenames.append(figfilename)
        futures.append(executor.submit(_animate, i, data, figfilename, fps))
    for future in futures:
        future.result()

    # Concatenate images into mp4 movie
    (
        ffmpeg
        .input(filename.parent/"*.png", pattern_type="glob", framerate=fps)
        .output(str(filename))
        .run()
     )
    for filename in filenames:
        filename.unlink()
    print(f"Wrote {filename}")
    return True


def _animate2(
        i: int,
        data: List[List[float]],  # time[channels[...]]
        filename: Path,
        fps: int,
        ) -> bool:
    # Find data statistics
    data = np.array(data).squeeze()
    if len(data.shape) < 2:
        data = np.expand_dims(data, -1)
    window_size = data.shape[-1]
    data = data.flatten().reshape(-1, 1)
    channel_mins = list()
    channel_maxs = list()
    for channel in data.T:
        channel_mins.append(min(channel))
        channel_maxs.append(max(channel))

    # Build plot figure
    timesteps, channels = data.shape
    px = 1/plt.rcParams['figure.dpi']
    fig, axs = plt.subplots(
            nrows=channels,
            figsize=(600*px, 400*px),
            squeeze=False,
            )
    axs = axs.flatten()
    fig.subplots_adjust(left=.16, right=.75, bottom=.02, top=.94, hspace=0.0)

    x = range(len(data))
    for ax, channel, axmin, axmax in zip(
            axs, data.T, channel_mins, channel_maxs
            ):
        ax.clear()
        ax.set_ylim(bottom=axmin, top=axmax)
        ax.set_yticks([])
        ax.set_xticks([])
        y = channel
        ax.plot(x, y)

    xstart = i*window_size
    xend = xstart+window_size
    for ax, channel, axmin, axmax in zip(
            axs, data.T, channel_mins, channel_maxs
            ):
        ax.set_xlim(left=xstart, right=xend)

    fig.savefig(filename)
    plt.close(fig)
    return True


def array_animation(
        data: List[List[float]],  # time[channels[...]]
        filename: Path,
        fps: int,
        executor: concurrent.futures.Executor,
        ) -> bool:
    if len(data) <= 1:
        print("Skipping bar animation because len(data) <= 1")
        return False
    # Plot all images in parallel
    futures = list()
    anim_id = str(uuid.uuid4())
    filenames = list()
    for i in range(len(data)):
        img_id = f"{i}".rjust(len(str(len(data))))
        figfilename = (filename.parent/(anim_id+img_id)).with_suffix(f".png")
        filenames.append(figfilename)
        futures.append(executor.submit(_animate, i, data, figfilename, fps))
    for future in futures:
        future.result()

    # Concatenate images into mp4 movie
    (
        ffmpeg
        .input(filename.parent/"*.png", pattern_type="glob", framerate=fps)
        .output(str(filename))
        .run()
     )
    for filename in filenames:
        filename.unlink()
    print(f"Wrote {filename}")
    return True


def barchart_animation(
        data: List[List[float]],  # time[channels[...]]
        filename: Path,
        fps: int,
        executor: concurrent.futures.Executor,
        ) -> bool:
    if len(data) <= 1:
        print("Skipping bar animation because len(data) <= 1")
        return False

    # Build plot figure
    px = 1/plt.rcParams['figure.dpi']
    fig, ax = plt.subplots(
            figsize=(600*px, 400*px),
            )
    fig.subplots_adjust(left=.06, right=.75, bottom=.02, top=.94, hspace=0.0)

    # Plot animation
    def animate(i):
        y = data[i]
        x = range(len(y))
        ax.clear()
        ax.bar(x, y)

    interval = 1000/fps
    ani = animation.FuncAnimation(
            fig,
            animate,
            frames=range(len(data)),
            interval=interval,
            blit=False,
        )
    ani.save(filename)
    print(f"Wrote {filename}")
    plt.close(fig)
    return True


def log_approaches_rewards(
        approaches_rewards: Dict[str, Dict[Task, float]],
        environment: str,
        log_dir: Path,
        ):
    def log_performance(tasks, sublog_dir):
        sublog_dir.mkdir()
        # Get the rewards on the given tasks
        task_rewards = {
                approach: [
                    rewards[task]
                    for task in rewards.keys()
                    if task in tasks
                ]
                for approach, rewards in approaches_rewards.items()
            }

        # Save raw data as JSON
        approach_rewards_json_path = sublog_dir/"approach_rewards.json"
        with open(approach_rewards_json_path, "wt") as fp:
            json.dump(task_rewards, fp)
            print(f"Wrote {approach_rewards_json_path}")

        keys = sorted(task_rewards.keys())
        values = [task_rewards[k] for k in keys]

        # Save raw data as CSV
        approach_rewards_csv_path = sublog_dir/"approach_rewards.csv"
        csv_dicts = [
                dict(chain(*list(map(lambda d: d.items(), [
                {
                    "APPROACH": key
                } , {
                    f"TERRAIN {i}": reward
                    for i, reward in enumerate(rewards)
                } , {
                    "MEAN": statistics.mean(rewards)
                } , {
                    "MEDIAN": statistics.mean(rewards)
                } , {
                    "STDEV": statistics.stdev(rewards)
                } , {
                    "MIN": min(rewards)
                } , {
                    "MAX": max(rewards)
                }]))))
                for key, rewards in task_rewards.items()
                if len(rewards) > 2
            ]
        if len(csv_dicts) > 0:
            with open(approach_rewards_csv_path, "wt") as fp:
                writer = csv.DictWriter(fp, csv_dicts[0].keys())
                writer.writeheader()
                writer.writerows(csv_dicts)
            print(f"Wrote {approach_rewards_csv_path}")

        # Save plots
        print(values)
        if not any(len(r) == 0 for r in values):
            fig_path = sublog_dir/"approach_rewards.svg"
            fig, ax = plt.subplots()
            ax.set_ylabel("Rewards")
            ax.violinplot(values)
            ax.set_xticks(range(1, len(keys)+1))
            ax.set_xticklabels(keys, rotation=90)
            plt.savefig(fig_path)
            plt.close(fig)
            print(f"Wrote {fig_path}")

    tasks = get_tasks(environment)
    log_performance(tasks.train, log_dir/"rewards_train")
    log_performance(tasks.test, log_dir/"rewards_test")
    log_performance(tasks, log_dir/"rewards_all")
