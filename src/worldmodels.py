"""World models."""
# Local imports
from policies import Policy

# Native imports
import abc
import tempfile
import os
import uuid
from pathlib import Path

# External imports
import gym


class WorldModel(abc.ABC):
    @property
    @abc.abstractmethod
    def state(self):
        pass

    @state.setter
    @abc.abstractmethod
    def state(self, value):
        pass

    @abc.abstractmethod
    def predict(self, env, observation, info, policy):
        """Returns (env, obs, reward, done, info)."""
        pass

    @abc.abstractmethod
    def step(self, a):
        pass


class PyBulletWorldModel(WorldModel):
    def __init__(self):
        self._state = None
        self._env = None
        _tmpdir = Path("./temp")
        _tmpdir.mkdir(parents=True, exist_ok=True)
        self.tmpdir = tempfile.TemporaryDirectory(dir=_tmpdir)
        self.tmpfilenames = list()

    @property
    def state(self):
        save_id = uuid.uuid4()
        tmpfilename = os.path.join(self.tmpdir.name, f"save_{save_id}.bullet")
        self._env._p.saveBullet(bulletFileName=tmpfilename)
        timesteps = self._env.timesteps
        last_x_positions = self._env.last_x_positions
        mean_vx = self._env.mean_vx
        self.tmpfilenames.append(tmpfilename)
        return (tmpfilename, timesteps, last_x_positions, mean_vx)

    @state.setter
    def state(self, value):
        tmpfilename, timesteps, last_x_positions, mean_vx = value
        self._env._p.restoreState(fileName=tmpfilename)
        self._env.last_x_positions = last_x_positions
        self._env.mean_vx = mean_vx
        self._env.timesteps = timesteps

    def predict(self, env, observation, info, policy):
        action = policy(env, observation, info)
        observation, reward, done, info = self._env.step(action)
        return env, observation, reward, done, info

    def step(self, a):
        observation, reward, done, info = self._env.step(a)
        return observation, reward, done, info


class WorldModelFiniteHorizonEnv(gym.Env):
    def __init__(
            self,
            initial_state,
            initial_observation,
            environment: str,
            horizon_len: int,
            finish_policy: Policy = None,
            finish_policy_timesteps: int = 0,
            ):
        self.environment = environment
        self.initial_state = initial_state
        self.initial_observation = initial_observation
        self.world_model = WorldModelFactory(environment)
        self.env = gym.make(environment)
        self.env.reset()
        self.world_model._env = self.env
        self.observation_space = self.env.observation_space
        self.action_space = self.env.action_space
        self.timesteps = 0
        self.horizon_len = horizon_len
        self.world_model.state = self.initial_state
        self.finish_policy = finish_policy
        self.finish_policy_timesteps = finish_policy_timesteps

    def reset(self):
        """Return to the initial state."""
        self.env.reset()
        self.world_model.state = self.initial_state
        self.timesteps = 0
        return self.initial_observation

    def step(self, *args, **kwargs):
        observation, reward, done, info = self.world_model.step(
                *args,
                **kwargs,
            )
        if self.timesteps == self.horizon_len:
            if self.finish_policy is not None:
                for i in range(self.finish_policy_timesteps):
                    if done:
                        break
                    a = self.finish_policy(self.env, observation, info)
                    _observation, _reward, _done, _ = self.world_model.step(a)
                    reward += _reward
            done = True
        self.timesteps += 1
        return observation, reward, done, info

    def close(self):
        self.env.close()


def WorldModelFactory(
        environment: str,
        ):
    if "bullet" in environment.lower():
        return PyBulletWorldModel()
    raise ValueError(f"No world model found for environment {environment}.")
