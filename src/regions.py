# Native imports
import abc
from typing import List
from typing import Tuple

# External imports
from sklearn.ensemble import IsolationForest
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import LocalOutlierFactor


Observation = Tuple[float, ...]


class SafetyPredictor(abc.ABC):
    """Abstract Base Class for success predictor. Success predictors model
    mappings from states to success probabilities for a particular policy."""
    @abc.abstractmethod
    def fit(self, observations: List[Tuple[Observation, bool]]) -> None:
        """Fit the model to the given tuples of observation and success."""
        pass

    @abc.abstractmethod
    def __call__(self, observation: Observation) -> float:
        """Return the prediction. Negative values represent unsafe
        observations. Positive values represent safe observations."""
        pass


class IsolationForestSafetyPredictor(SafetyPredictor):
    def __init__(
            self,
            *args,
            **kwargs,
            ):
        self.tree = IsolationForest(n_jobs=-1, n_estimators=20)
        super().__init__(*args, **kwargs)

    def fit(self, observations: List[Tuple[Observation, bool]]) -> None:
        # We only care about positive examples in this case.
        X = [x for x, success in observations if success]
        self.tree.fit(X)

    def __call__(self, observation: Observation) -> float:
        return self.tree.predict([observation]).squeeze()


class LocalOutlierFactorSafetyPredictor(SafetyPredictor):
    def __init__(
            self,
            *args,
            **kwargs,
            ):
        self.model = LocalOutlierFactor(n_jobs=-1, novelty=True)
        super().__init__(*args, **kwargs)

    def fit(self, observations: List[Tuple[Observation, bool]]) -> None:
        # We only care about positive examples in this case.
        X = [x for x, success in observations if success]
        self.model.fit(X)

    def __call__(self, observation: Observation) -> float:
        return self.model.decision_function([observation]).squeeze()


class DecisionTreeSafetyPredictor(SafetyPredictor):
    def __init__(
            self,
            *args,
            **kwargs,
            ):
        self.tree = DecisionTreeClassifier()
        super().__init__(*args, **kwargs)

    def fit(self, observations: List[Tuple[Observation, bool]]) -> None:
        safe_observations = [
                obs for obs, obs_is_safe in observations
                if obs_is_safe
            ]
        unsafe_observations = [
                obs for obs, obs_is_safe in observations
                if not obs_is_safe
            ]
        X = list()
        Y = list()
        X.extend(safe_observations)
        Y.extend([1]*len(safe_observations))
        X.extend(unsafe_observations)
        Y.extend([0]*len(unsafe_observations))
        self.tree.fit(X, Y)

    def __call__(self, observation: Observation) -> float:
        probabilty_safety = self.tree.predict_proba([observation]).squeeze()[1]
        safety = probabilty_safety*2 - 1
        return safety
